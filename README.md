PLA - Power TAC Logfile Analysis (PLA) v2.0
10/02/2020, Serkan Ozdemir, serkan.ozdemir@uni-due.de

This project extends the original version of the PLA developed by Markus Peters 
(publicly available on https://bitbucket.org/markuspeters/pla). This version of the PLA 
includes various bug fixes and improvements. As of 10.02.2020, it is compatible with the
latest stable Power TAC server (1.6.0).
This project includes all the necessary scripts and documentation to build your analysis
tool. Please refer to the pla2_handbook.pdf before you get started.

******************************************************************************************

Download PLA v2.0

PLA v2.0 is publicly accessible on https://bitbucket.org/serkan-ozdemir/pla2. You need a git
client to download the project files:

    git clone git@bitbucket.org:serkan-ozdemir/pla2.git
    
This command creates a directory called pla2 and various subdirectories.

******************************************************************************************

Requirements:

- A Linux-based operating system. 
- MySQL DBMS.
- A MySQL DBMS client, preferably phpMyAdmin.
- PHP 5.4+ 

******************************************************************************************

Quick Installation (see pla2_handbook.pdf for more detail):

- The original state log files must be renamed using the template powertac-sim-INTEGER.state where “INTEGER” is the index of the state file and must be replaced by a positive integer value. If multiple state log files need to be imported, then consecutive numbers (starting from 1) must be used.  
- Navigate to the script folder and execute "sh ./process-all-logs.sh START END" where “START” and “END” denote the first and last index of the state log files. They must be replaced by positive integer values.
- Import "placreate.sql" to your database using a MySQL client.
- Navigate to the script folder and execute "php ./import-to-db.php START END" where “START” and “END” denote the first and last index of the state log files, as explained above.

******************************************************************************************

Database Model:

The database model and various EER diagrams can be reviewed in <pla2.mwb>, can be opened with MySQL Workbench.
