#!/bin/bash

# Please read pla2_handbook.pdf before you get started.
# Following piece of code processes all state log files and store them in the "processed" folder.
# To execute the script, navigate to the script folder.
# Usage: sh ./process-all-logs.sh START END // example: sh ./process-all-logs.sh 1 25
# Replace "START" with a positive integer value // counter: index of the first state log file. 
# Replace "END" with a positive integer value // counter: index of the last state log file. 
# 10/02/2020, Serkan Ozdemir, serkan.ozdemir@uni-due.de

counter=$1
games=$2

while [ "$counter" -le "$games" ]
do
	# remove "#" below if you want to process boot log files.
    # echo "boot state file $counter is being processed";
	# sh ./processlogs.sh logs/powertac-boot-"$counter".state 2013 > processed-logs/powertac-boot-"$counter".processed

	echo "sim state file $counter is being processed";
	sh ./processlogs.sh logs/powertac-sim-"$counter".state 2013 > processed-logs/powertac-sim-"$counter".processed


    ((counter++))
done
