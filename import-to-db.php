<?php

# Please read pla2_handbook.pdf before you get started.
# Following piece of code imports all processed log files into the database.
# To execute the script, navigate to the script folder.
# Usage: php ./import-to-db.php START END // example: php ./import-to-db.php 1 25
# Replace "START" with a positive integer value // counter: index of the first state log file. 
# Replace "END" with a positive integer value // counter: index of the last state log file. 
# Please be patient during the import process. The import process of each log file takes approximately 5 - 15 minutes.
# 10/02/2020, Serkan Ozdemir, serkan.ozdemir@uni-due.de

if(ini_set('max_execution_time', 0))
echo "time set to unlimited\n";
if(set_time_limit(0))
echo "time set to unlimited\n";


$database = "pla_db_name";
$username = "root";
$password = "xxxxx";
$hostname = "localhost"; 
$dbhandle = mysql_connect($hostname, $username, $password) or die("Unable to connect to MySQL");
$selected = mysql_select_db($database,$dbhandle) or die("Could not select examples");



$counter = $argv[1]; // index of the first state log file. 
$games = $argv[2]; // index of the last state log file.


while($counter <= $games)
{
	// Sim logs
	mysql_query('TRUNCATE TABLE pla_import;');
	echo "Importing powertac-sim-".$counter.".processed \n";
	mysql_query('LOAD DATA LOCAL
	INFILE "'.getcwd().'/processed/powertac-sim-'.$counter.'.processed"
	REPLACE
	INTO TABLE '.$database.'.pla_import
	FIELDS TERMINATED BY "::"
	LINES TERMINATED BY "\n"
	(imp_ts, imp_time, imp_class, imp_instance_id, imp_method,
	imp_arg1, imp_arg2, imp_arg3, imp_arg4,
	imp_arg5, imp_arg6, imp_arg7, imp_arg8,
	imp_arg9, imp_arga, imp_argb, imp_argc,
	imp_argd);');
	mysql_query('CALL '.$database.'.import("sim-'.$counter.'", TRUE, "2013");');
	$counter++;
} 
mysql_query('TRUNCATE TABLE pla_import;');

?>
