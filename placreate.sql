SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `pla_competition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_competition` ;

CREATE  TABLE IF NOT EXISTS `pla_competition` (
  `cmp_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cmp_name` VARCHAR(256) NULL DEFAULT NULL,
  PRIMARY KEY (`cmp_id`) )
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `pla_competition_properties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_competition_properties` ;

CREATE  TABLE IF NOT EXISTS `pla_competition_properties` (
  `cpr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cpr_name` VARCHAR(256) NULL DEFAULT NULL ,
  `cpr_value` VARCHAR(256) NULL DEFAULT NULL ,
  `cpr_competition` INT(11) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`cpr_id`) ,
  INDEX `fk_cpr_cmp_idx` (`cpr_competition` ASC) ,
  UNIQUE INDEX `uk_cpr_id_name` (`cpr_name` ASC, `cpr_id` ASC) ,
  CONSTRAINT `fk_cpr_cmp`
    FOREIGN KEY (`cpr_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `pla_import`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_import` ;

CREATE  TABLE IF NOT EXISTS `pla_import` (
  `imp_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `imp_ts` INT UNSIGNED NULL ,
  `imp_time` INT NOT NULL ,
  `imp_class` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_instance_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `imp_method` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg1` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg2` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg3` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg4` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg5` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg6` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg7` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg8` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg9` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arga` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_argb` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_argc` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_argd` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_target` INT(11) UNSIGNED NULL DEFAULT NULL ,
  INDEX `fk_imp_cmp_idx` (`imp_target` ASC) ,
  PRIMARY KEY (`imp_id`) ,
  INDEX `idx_imp_class` (`imp_class` ASC) ,
  INDEX `idx_imp_target` (`imp_target` ASC) ,
  INDEX `idx_imp_instance_id` (`imp_instance_id` ASC) ,
  INDEX `idx_tmp_ts` (`imp_ts` ASC) ,
  INDEX `index8` (`imp_class` ASC, `imp_method` ASC) ,
  CONSTRAINT `fk_imp_cmp`
    FOREIGN KEY (`imp_target` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `pla_timeslot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_timeslot` ;

CREATE  TABLE IF NOT EXISTS `pla_timeslot` (
  `tsl_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tsl_competition` INT(11) UNSIGNED NOT NULL ,
  `tsl_serial` INT NOT NULL ,
  `tsl_time` DATETIME NOT NULL ,
  `tsl_timestring` VARCHAR(256) NOT NULL,
  UNIQUE INDEX `uk_tsl_serial_competition` (`tsl_competition` ASC, `tsl_serial` ASC) ,
  INDEX `fk_tsl_cmp_idx` (`tsl_competition` ASC) ,
  PRIMARY KEY (`tsl_id`) ,
  INDEX `idx_tsl_time` (`tsl_time` ASC) ,
  INDEX `idx_tsl_timestring` (`tsl_timestring` ASC) ,
  INDEX `idx_tsl_serial` (`tsl_serial` ASC) ,
  CONSTRAINT `fk_tsl_cmp`
    FOREIGN KEY (`tsl_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_customer` ;

CREATE  TABLE IF NOT EXISTS `pla_customer` (
  `cst_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cst_competition` INT(11) UNSIGNED NOT NULL ,
  `cst_instance` INT(11) UNSIGNED NOT NULL ,
  `cst_population` INT NOT NULL,
  `cst_name` VARCHAR(256) NOT NULL,
  `cst_powertype` VARCHAR(256) NULL DEFAULT NULL,
  `cst_multi_contracting` TINYINT(1) NULL DEFAULT FALSE,
  `cst_can_negotiate` TINYINT(1) NULL DEFAULT FALSE,
  PRIMARY KEY (`cst_id`) ,
  INDEX `fk_cst_competition_idx` (`cst_competition` ASC) ,
  INDEX `idx_cst_instance` (`cst_instance` ASC) ,
  UNIQUE INDEX `uk_cst_instance_competition` (`cst_competition` ASC, `cst_instance` ASC) ,
  CONSTRAINT `fk_cst_competition`
    FOREIGN KEY (`cst_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_broker`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_broker` ;

CREATE  TABLE IF NOT EXISTS `pla_broker` (
  `bkr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bkr_instance` INT(11) UNSIGNED NOT NULL ,
  `bkr_competition` INT(11) UNSIGNED NOT NULL ,
  `bkr_name` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`bkr_id`) ,
  INDEX `fk_bkr_cmp_idx` (`bkr_competition` ASC) ,
  UNIQUE INDEX `uk_bkr_instance_competition` (`bkr_instance` ASC, `bkr_competition` ASC) ,
  INDEX `idx_bkr_competition` (`bkr_competition` ASC) ,
  CONSTRAINT `fk_bkr_cmp`
    FOREIGN KEY (`bkr_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_tariff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_tariff` ;

CREATE  TABLE IF NOT EXISTS `pla_tariff` (
  `trf_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `trf_instance` INT(11) UNSIGNED NOT NULL ,
  `trf_broker` INT(11) UNSIGNED NOT NULL ,
  `trf_competition` INT(11) UNSIGNED NOT NULL ,
  `trf_minDuration` DECIMAL(20,0) NULL,
  `trf_signUpPayment` double NULL,
  `trf_earlyWithdrawPayment` double NULL,
  `trf_periodicPayment` double NULL,
  `trf_powerType` varchar(255) NOT NULL,
  PRIMARY KEY (`trf_id`) ,
  INDEX `fk_trf_bkr_idx` (`trf_broker` ASC) ,
  INDEX `fk_trf_cmp_idx` (`trf_competition` ASC) ,
  UNIQUE INDEX `uk_trf_instance_competition` (`trf_instance` ASC, `trf_competition` ASC) ,
  INDEX `idx_trf_competition` (`trf_competition` ASC) ,
  INDEX `idx_trf_instance` (`trf_instance` ASC) ,
  CONSTRAINT `fk_trf_bkr`
    FOREIGN KEY (`trf_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_trf_cmp`
    FOREIGN KEY (`trf_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_tariff_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_tariff_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla_tariff_transaction` (
  `tfc_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tfc_instance` INT(11) UNSIGNED NOT NULL ,
  `tfc_customer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `tfc_tariff` INT(11) UNSIGNED NOT NULL ,
  `tfc_timeslot` INT(11) UNSIGNED NOT NULL ,
  `tfc_customer_count` INT NULL DEFAULT NULL ,
  `tfc_kwh` DECIMAL(15,5) NOT NULL ,
  `tfc_charge` DECIMAL(15,5) NOT NULL ,
  `tfc_type` VARCHAR(256) NOT NULL ,
  `tfc_regulation` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`tfc_id`) ,
  INDEX `fk_tfc_cst_idx` (`tfc_customer` ASC) ,
  INDEX `fk_tfc_trf_idx` (`tfc_tariff` ASC) ,
  INDEX `fk_tfc_tsl_idx` (`tfc_timeslot` ASC) ,
  INDEX `idx_tfc` (`tfc_instance` ASC) ,
  INDEX `idx_tfc_type` (`tfc_type` ASC) ,
  CONSTRAINT `fk_tfc_cst`
    FOREIGN KEY (`tfc_customer` )
    REFERENCES `pla_customer` (`cst_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tfc_trf`
    FOREIGN KEY (`tfc_tariff` )
    REFERENCES `pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tfc_tsl`
    FOREIGN KEY (`tfc_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_weather_forecast`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_weather_forecast` ;

CREATE  TABLE IF NOT EXISTS `pla_weather_forecast` (
  `wfc_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wfc_instance` INT(11) UNSIGNED NOT NULL ,
  `wfc_data` VARCHAR(256) NOT NULL ,
  `wfc_competition` INT(11) UNSIGNED NOT NULL ,
  `wfc_timeslot` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`wfc_id`) ,
  INDEX `fk_wfc_cmp_idx` (`wfc_competition` ASC) ,
  INDEX `fk_wfc_tsl_idx` (`wfc_timeslot` ASC) ,
  INDEX `idx_wfc_instance` (`wfc_instance` ASC) ,
  UNIQUE INDEX `uk_wfc_instance_competition` (`wfc_instance` ASC, `wfc_competition` ASC) ,
  CONSTRAINT `fk_wfc_cmp`
    FOREIGN KEY (`wfc_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wfc_tsl`
    FOREIGN KEY (`wfc_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_weather_forecast_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_weather_forecast_data` ;

CREATE  TABLE IF NOT EXISTS `pla_weather_forecast_data` (
  `wfd_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wfd_instance` INT(11) UNSIGNED NOT NULL ,
  `wfd_competition` INT(11) UNSIGNED NOT NULL ,
  `wfd_offset` INT NOT NULL ,
  `wfd_temperature` DECIMAL(15,5) NOT NULL ,
  `wfd_wind_speed` DECIMAL(15,5) NOT NULL ,
  `wfd_wind_direction` DECIMAL(15,5) NOT NULL ,
  `wfd_cloud_cover` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`wfd_id`) ,
  INDEX `idx_wfd_instance` (`wfd_instance` ASC)
)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_orderbook`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_orderbook` ;

CREATE  TABLE IF NOT EXISTS `pla_orderbook` (
  `obk_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `obk_instance` INT(11) UNSIGNED NOT NULL ,
  `obk_competition` INT(11) UNSIGNED NOT NULL ,
  `obk_timeslot` INT(11) UNSIGNED NOT NULL ,
  `obk_clearing_price` DECIMAL(15,5) NOT NULL ,
  `obk_date_executed` DATETIME NULL ,
  PRIMARY KEY (`obk_id`) ,
  INDEX `fk_obk_cmp_idx` (`obk_competition` ASC) ,
  INDEX `fk_obk_tsl_idx` (`obk_timeslot` ASC) ,
  INDEX `idx_obk_instance` (`obk_instance` ASC) ,
  UNIQUE INDEX `uk_obk_instance_competition` (`obk_instance` ASC, `obk_competition` ASC) ,
  CONSTRAINT `fk_obk_cmp`
    FOREIGN KEY (`obk_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_obk_tsl`
    FOREIGN KEY (`obk_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_orderbook_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_orderbook_order` ;

CREATE  TABLE IF NOT EXISTS `pla_orderbook_order` (
  `obo_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `obo_instance` INT(11) UNSIGNED NOT NULL ,
  `obo_competition` INT(11) UNSIGNED NOT NULL ,
  `obo_orderbook` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `obo_mwh` DECIMAL(15,5) NOT NULL ,
  `obo_limit_price` DECIMAL(15,5) NOT NULL ,
  `obo_type` ENUM('ASK','BID') NULL DEFAULT NULL ,
  PRIMARY KEY (`obo_id`) ,
  INDEX `fk_obo_orderbook_idx` (`obo_orderbook` ASC) ,
  INDEX `idx_obo_instance` (`obo_instance` ASC) ,
  INDEX `fk_obo_competition_idx` (`obo_competition` ASC) ,
  CONSTRAINT `fk_obo_orderbook`
    FOREIGN KEY (`obo_orderbook` )
    REFERENCES `pla_orderbook` (`obk_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_obo_competition`
    FOREIGN KEY (`obo_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_buyer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_buyer` ;

CREATE  TABLE IF NOT EXISTS `pla_buyer` (
  `buy_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `buy_instance` INT(11) UNSIGNED NOT NULL ,
  `buy_competition` INT(11) UNSIGNED NOT NULL ,
  `buy_name` VARCHAR(256) NOT NULL ,
  `buy_price_beta` DECIMAL(15,5) NULL DEFAULT NULL ,
  `buy_mwh` DECIMAL(15,5) NULL DEFAULT NULL ,
  PRIMARY KEY (`buy_id`) ,
  INDEX `fk_buy_cmp_idx` (`buy_competition` ASC) ,
  UNIQUE INDEX `uk_buy_instance_competition` (`buy_instance` ASC, `buy_competition` ASC) ,
  INDEX `idx_buy_instance` (`buy_instance` ASC) ,
  CONSTRAINT `fk_buy_cmp`
    FOREIGN KEY (`buy_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_genco`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_genco` ;

CREATE  TABLE IF NOT EXISTS `pla_genco` (
  `gnc_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `gnc_instance` INT(11) UNSIGNED NOT NULL ,
  `gnc_competition` INT(11) UNSIGNED NOT NULL ,
  `gnc_name` VARCHAR(256) NOT NULL ,
  `gnc_nominal_capacity` DECIMAL(15,5) NULL DEFAULT NULL,
  `gnc_variability` DECIMAL(15,5) NULL DEFAULT NULL,
  `gnc_reliability` DECIMAL(15,5) NULL DEFAULT NULL,
  `gnc_cost` DECIMAL(15,5) NULL DEFAULT NULL,
  `gnc_carbon_emission_rate` DECIMAL(15,5) NULL DEFAULT NULL,
  `gnc_commitment_leadtime` DECIMAL(15,5) NULL DEFAULT NULL,
  PRIMARY KEY (`gnc_id`) ,
  INDEX `fk_gnc_cmp_idx` (`gnc_competition` ASC) ,
  UNIQUE INDEX `uk_gnc_instance_competition` (`gnc_instance` ASC, `gnc_competition` ASC) ,
  INDEX `idx_gnc_instance` (`gnc_instance` ASC) ,
  CONSTRAINT `fk_gnc_cmp`
    FOREIGN KEY (`gnc_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;



-- -----------------------------------------------------
-- Table `pla_hydrogen_station`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_hydrogen_station` ;

CREATE  TABLE IF NOT EXISTS `pla_hydrogen_station` (
  `hyd_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `hyd_instance` INT(11) UNSIGNED NOT NULL ,
  `hyd_customer_instance` INT(11) UNSIGNED NOT NULL ,
  `hyd_competition` INT(11) UNSIGNED NOT NULL ,
  `hyd_name` VARCHAR(256) NOT NULL ,
  `hyd_electrolyzer_capacity` INT(11) NULL DEFAULT NULL,
  `hyd_hydrogen_storage_capacity` INT(11) NULL DEFAULT NULL,
  `hyd_customer_population` INT(11) NULL DEFAULT NULL,
  `hyd_dss_method` INT(11) NULL DEFAULT NULL,
  `hyd_custom_factor` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`hyd_id`) ,
  INDEX `fk_hyd_cmp_idx` (`hyd_competition` ASC) ,
  UNIQUE INDEX `uk_hyd_instance_competition` (`hyd_instance` ASC, `hyd_competition` ASC) ,
  INDEX `idx_hyd_instance` (`hyd_instance` ASC) ,
  CONSTRAINT `fk_hyd_cmp`
    FOREIGN KEY (`hyd_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_hydrogen_station_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_hydrogen_station_state` ;

CREATE  TABLE IF NOT EXISTS `pla_hydrogen_station_state` (
  `hys_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `hys_hydrogen_station` INT(11) UNSIGNED NOT NULL ,
  `hys_timeslot` INT(11) UNSIGNED NOT NULL,
  `hys_state_storage` DECIMAL(15,5) NULL DEFAULT NULL,
  `hys_produced_hydrogen` DECIMAL(15,5) NULL DEFAULT NULL,
  `hys_consumed_hydrogen` DECIMAL(15,5) NULL DEFAULT NULL,
  PRIMARY KEY (`hys_id`) ,
  INDEX `fk_hys_hyd_idx` (`hys_hydrogen_station` ASC) ,
  INDEX `fk_hys_tsl_idx` (`hys_timeslot` ASC) ,
  CONSTRAINT `fk_hys_hyd`
    FOREIGN KEY (`hys_hydrogen_station` )
    REFERENCES `pla_hydrogen_station` (`hyd_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_hys_tsl`
    FOREIGN KEY (`hys_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_market_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_market_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla_market_transaction` (
  `mtr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `mtr_instance` INT(11) UNSIGNED NOT NULL ,
  `mtr_competition` INT(11) UNSIGNED NOT NULL ,
  `mtr_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mtr_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mtr_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mtr_delivery_timeslot` INT(11) UNSIGNED NOT NULL ,
  `mtr_order_timeslot` INT(11) UNSIGNED NOT NULL ,
  `mtr_mwh` DECIMAL(15,5) NOT NULL ,
  `mtr_price` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`mtr_id`) ,
  INDEX `fk_mtr_cmp_idx` (`mtr_competition` ASC) ,
  INDEX `fk_mtr_bkr_idx` (`mtr_broker` ASC) ,
  INDEX `fk_mtr_tsl_idx` (`mtr_delivery_timeslot` ASC) ,
  INDEX `idx_mtr_instance` (`mtr_instance` ASC) ,
  UNIQUE INDEX `uk_mtr_instance_competition` (`mtr_instance` ASC, `mtr_competition` ASC) ,
  INDEX `fk_mtr_buy_idx` (`mtr_buyer` ASC) ,
  INDEX `fk_mtr_gnc_idx` (`mtr_genco` ASC) ,
  INDEX `fk_mtr_order_tsl_idx` (`mtr_order_timeslot` ASC) ,
  CONSTRAINT `fk_mtr_cmp`
    FOREIGN KEY (`mtr_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_bkr`
    FOREIGN KEY (`mtr_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_delivery_tsl`
    FOREIGN KEY (`mtr_delivery_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_buy`
    FOREIGN KEY (`mtr_buyer` )
    REFERENCES `pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_gnc`
    FOREIGN KEY (`mtr_genco` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_order_tsl`
    FOREIGN KEY (`mtr_order_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_cash_position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_cash_position` ;

CREATE  TABLE IF NOT EXISTS `pla_cash_position` (
  `cpo_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cpo_instance` INT(11) UNSIGNED NOT NULL ,
  `cpo_competition` INT(11) UNSIGNED NOT NULL ,
  `cpo_timeslot` INT(11) UNSIGNED NOT NULL ,
  `cpo_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `cpo_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `cpo_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `cpo_balance` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`cpo_id`) ,
  INDEX `fk_cpo_bkr_idx` (`cpo_broker` ASC) ,
  INDEX `fk_cpo_cmp_idx` (`cpo_competition` ASC) ,
  INDEX `idx_cpo_instance` (`cpo_instance` ASC) ,
  INDEX `fk_cpo_gnc_idx` (`cpo_genco` ASC) ,
  INDEX `fk_cpo_buy_idx` (`cpo_buyer` ASC) ,
  INDEX `fk_cpo_tsl_idx` (`cpo_timeslot` ASC) ,
  CONSTRAINT `fk_cpo_bkr`
    FOREIGN KEY (`cpo_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_cmp`
    FOREIGN KEY (`cpo_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_gnc`
    FOREIGN KEY (`cpo_genco` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_buy`
    FOREIGN KEY (`cpo_buyer` )
    REFERENCES `pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_tsl`
    FOREIGN KEY (`cpo_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_market_position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_market_position` ;

CREATE  TABLE IF NOT EXISTS `pla_market_position` (
  `mpo_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `mpo_instance` INT(11) UNSIGNED NOT NULL ,
  `mpo_competition` INT(11) UNSIGNED NOT NULL ,
  `mpo_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mpo_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mpo_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mpo_timeslot` INT(11) UNSIGNED NOT NULL ,
  `mpo_initial` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`mpo_id`) ,
  INDEX `fk_mpo_bkr_idx` (`mpo_broker` ASC) ,
  INDEX `fk_mpo_cmp_idx` (`mpo_competition` ASC) ,
  INDEX `idx_mpo_instance` (`mpo_instance` ASC) ,
  INDEX `fk_mpo_tsl_idx` (`mpo_timeslot` ASC) ,
  UNIQUE INDEX `uk_mpo_instance_competition` (`mpo_instance` ASC, `mpo_competition` ASC) ,
  INDEX `fk_mpo_gnc_idx` (`mpo_genco` ASC) ,
  INDEX `fk_mpo_buy_idx` (`mpo_buyer` ASC) ,
  CONSTRAINT `fk_mpo_bkr`
    FOREIGN KEY (`mpo_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_cmp`
    FOREIGN KEY (`mpo_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_tsl`
    FOREIGN KEY (`mpo_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_gnc`
    FOREIGN KEY (`mpo_genco` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_buy`
    FOREIGN KEY (`mpo_buyer` )
    REFERENCES `pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_market_position_update`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_market_position_update` ;

CREATE  TABLE IF NOT EXISTS `pla_market_position_update` (
  `mpu_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `mpu_market_position` INT(11) UNSIGNED NOT NULL ,
  `mpu_timeslot` INT(11) UNSIGNED NOT NULL ,
  `mpu_mwh` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`mpu_id`) ,
  INDEX `fk_mpd_mpo_idx` (`mpu_market_position` ASC) ,
  INDEX `fk_mpu_tsl_idx` (`mpu_timeslot` ASC) ,
  CONSTRAINT `fk_mpd_mpo`
    FOREIGN KEY (`mpu_market_position` )
    REFERENCES `pla_market_position` (`mpo_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpu_tsl`
    FOREIGN KEY (`mpu_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_rate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_rate` ;

CREATE  TABLE IF NOT EXISTS `pla_rate` (
  `rte_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rte_instance` INT(11) UNSIGNED NOT NULL ,
  `rte_competition` INT(11) UNSIGNED NOT NULL ,
  `rte_tariff` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `rte_weekly_begin` INT NULL DEFAULT NULL ,
  `rte_weekly_end` INT NULL DEFAULT NULL ,
  `rte_daily_begin` INT NULL DEFAULT NULL ,
  `rte_daily_end` INT NULL DEFAULT NULL ,
  `rte_tier_threshold` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_fixed` TINYINT(1) NULL DEFAULT TRUE ,
  `rte_min_value` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_max_value` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_notice_interval` INT NULL DEFAULT NULL ,
  `rte_expected_mean` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_max_curtailment` DECIMAL(15,5) NULL DEFAULT NULL ,
  PRIMARY KEY (`rte_id`) ,
  INDEX `fk_rte_trf_idx` (`rte_tariff` ASC) ,
  INDEX `idx_rte_instance` (`rte_instance` ASC) ,
  INDEX `fk_rte_cmp_idx` (`rte_competition` ASC) ,
  CONSTRAINT `fk_rte_trf`
    FOREIGN KEY (`rte_tariff` )
    REFERENCES `pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rte_cmp`
    FOREIGN KEY (`rte_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_weather_report`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_weather_report` ;

CREATE  TABLE IF NOT EXISTS `pla_weather_report` (
  `wre_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wre_instance` INT(11) UNSIGNED NOT NULL ,
  `wre_competition` INT(11) UNSIGNED NOT NULL ,
  `wre_timeslot` INT(11) UNSIGNED NOT NULL ,
  `wre_temperature` DECIMAL(15,5) NOT NULL ,
  `wre_wind_speed` DECIMAL(15,5) NOT NULL ,
  `wre_wind_direction` DECIMAL(15,5) NOT NULL ,
  `wre_cloud_cover` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`wre_id`) ,
  INDEX `fk_wre_cmp_idx` (`wre_competition` ASC) ,
  INDEX `fk_wre_tsl_idx` (`wre_timeslot` ASC) ,
  INDEX `idx_wre_instance` (`wre_instance` ASC) ,
  CONSTRAINT `fk_wre_cmp`
    FOREIGN KEY (`wre_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_wre_tsl`
    FOREIGN KEY (`wre_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_order` ;

CREATE  TABLE IF NOT EXISTS `pla_order` (
  `ord_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ord_instance` INT(11) UNSIGNED NOT NULL ,
  `ord_competition` INT(11) UNSIGNED NOT NULL ,
  `ord_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `ord_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `ord_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `ord_timeslot` INT(11) UNSIGNED NOT NULL ,
  `ord_timeslot_placed` INT(11) UNSIGNED NOT NULL ,
  `ord_mwh` DECIMAL(15,5) NOT NULL ,
  `ord_limit` DECIMAL(15,5) NULL ,
  PRIMARY KEY (`ord_id`) ,
  INDEX `fk_ord_cmp_idx` (`ord_competition` ASC) ,
  INDEX `fk_ord_bkr_idx` (`ord_broker` ASC) ,
  INDEX `fk_ord_tsl_order_idx` (`ord_timeslot` ASC) ,
  INDEX `idx_ord_instance` (`ord_instance` ASC) ,
  UNIQUE INDEX `uk_ord_instance_competition` (`ord_instance` ASC, `ord_competition` ASC) ,
  INDEX `fk_ord_gnc_idx` (`ord_genco` ASC) ,
  INDEX `fk_ord_buy_idx` (`ord_buyer` ASC) ,
  INDEX `fk_ord_tsl_placed_idx` (`ord_timeslot_placed` ASC) ,
  CONSTRAINT `fk_ord_cmp`
    FOREIGN KEY (`ord_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_bkr`
    FOREIGN KEY (`ord_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_tsl_order`
    FOREIGN KEY (`ord_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_gnc`
    FOREIGN KEY (`ord_genco` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_buy`
    FOREIGN KEY (`ord_buyer` )
    REFERENCES `pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_tsl_placed`
    FOREIGN KEY (`ord_timeslot_placed` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_cleared_trade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_cleared_trade` ;

CREATE  TABLE IF NOT EXISTS `pla_cleared_trade` (
  `clt_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `clt_instance` INT(11) UNSIGNED NOT NULL ,
  `clt_competition` INT(11) UNSIGNED NOT NULL ,
  `clt_timeslot` INT(11) UNSIGNED NOT NULL ,
  `clt_execution_mwh` DECIMAL(15,5) NOT NULL ,
  `clt_execution_price` DECIMAL(15,5) NOT NULL ,
  `clt_date_executed` DATETIME NOT NULL ,
  PRIMARY KEY (`clt_id`) ,
  INDEX `fk_clt_cmp_idx` (`clt_competition` ASC) ,
  INDEX `fk_clt_tsl_idx` (`clt_timeslot` ASC) ,
  INDEX `idx_clt_instance` (`clt_instance` ASC) ,
  CONSTRAINT `fk_clt_cmp`
    FOREIGN KEY (`clt_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_clt_tsl`
    FOREIGN KEY (`clt_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_balancing_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_balancing_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla_balancing_transaction` (
  `btr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `btr_instance` INT(11) UNSIGNED NOT NULL ,
  `btr_competition` INT(11) UNSIGNED NOT NULL ,
  `btr_broker` INT(11) UNSIGNED NOT NULL ,
  `btr_timeslot` INT(11) UNSIGNED NOT NULL ,
  `btr_kwh` DECIMAL(15,5) NOT NULL ,
  `btr_charge` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`btr_id`) ,
  INDEX `fk_btr_cmp_idx` (`btr_competition` ASC) ,
  INDEX `fk_btr_bkr_idx` (`btr_broker` ASC) ,
  INDEX `idx_btr_instance` (`btr_instance` ASC) ,
  INDEX `idx_btr_time` (`btr_timeslot` ASC) ,
  UNIQUE INDEX `uk_btr_instance_competition` (`btr_instance` ASC, `btr_competition` ASC) ,
  INDEX `fk_btr_tsl_idx` (`btr_timeslot` ASC) ,
  CONSTRAINT `fk_btr_cmp`
    FOREIGN KEY (`btr_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_btr_bkr`
    FOREIGN KEY (`btr_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_btr_tsl`
    FOREIGN KEY (`btr_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_distribution_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_distribution_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla_distribution_transaction` (
  `dtr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `dtr_instance` INT(11) UNSIGNED NOT NULL ,
  `dtr_competition` INT(11) UNSIGNED NOT NULL ,
  `dtr_broker` INT(11) UNSIGNED NOT NULL ,
  `dtr_timeslot` INT(11) UNSIGNED NOT NULL ,
  `dtr_kwh` DECIMAL(15,5) NOT NULL,
  `dtr_charge` DECIMAL(15,5) NOT NULL,
  PRIMARY KEY (`dtr_id`) ,
  INDEX `fk_dtr_cmp_idx` (`dtr_competition` ASC) ,
  INDEX `fk_dtr_bkr_idx` (`dtr_broker` ASC) ,
  INDEX `idx_dtr_instance` (`dtr_instance` ASC) ,
  INDEX `idx_dtr_time` (`dtr_timeslot` ASC) ,
  UNIQUE INDEX `uk_dtr_instance_competition` (`dtr_instance` ASC, `dtr_competition` ASC) ,
  INDEX `fk_dtr_tsl_idx` (`dtr_timeslot` ASC) ,
  CONSTRAINT `fk_dtr_cmp`
    FOREIGN KEY (`dtr_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_dtr_bkr`
    FOREIGN KEY (`dtr_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_dtr_tsl`
    FOREIGN KEY (`dtr_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_genco_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_genco_state` ;

CREATE  TABLE IF NOT EXISTS `pla_genco_state` (
  `gcs_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `gcs_genco` INT(11) UNSIGNED NOT NULL ,
  `gcs_timeslot` INT(11) UNSIGNED NOT NULL ,
  `gcs_current_capacity` DECIMAL(15,5) NULL DEFAULT NULL,
  `gcs_in_operation` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`gcs_id`) ,
  INDEX `fk_gcs_gnc_idx` (`gcs_genco` ASC) ,
  INDEX `fk_gcs_tsl_idx` (`gcs_timeslot` ASC) ,
  CONSTRAINT `fk_gcs_gnc`
    FOREIGN KEY (`gcs_genco` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_gcs_tsl`
    FOREIGN KEY (`gcs_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_buyer_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_buyer_state` ;

CREATE  TABLE IF NOT EXISTS `pla_buyer_state` (
  `bys_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bys_buyer` INT(11) UNSIGNED NOT NULL ,
  `bys_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bys_current_capacity` DECIMAL(15,5) NULL DEFAULT NULL ,
  `bys_in_operation` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`bys_id`) ,
  INDEX `fk_bys_gnc_idx` (`bys_buyer` ASC) ,
  INDEX `fk_bys_tsl_idx` (`bys_timeslot` ASC) ,
  CONSTRAINT `fk_bys_gnc`
    FOREIGN KEY (`bys_buyer` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bys_tsl`
    FOREIGN KEY (`bys_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_bank_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_bank_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla_bank_transaction` (
  `bnt_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bnt_instance` INT(11) UNSIGNED NOT NULL ,
  `bnt_competition` INT(11) UNSIGNED NOT NULL ,
  `bnt_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `bnt_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `bnt_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `bnt_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bnt_amount` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`bnt_id`) ,
  INDEX `fk_bnt_cmp_idx` (`bnt_competition` ASC) ,
  INDEX `fk_bnt_bkr_idx` (`bnt_broker` ASC) ,
  INDEX `fk_bnt_tsl_idx` (`bnt_timeslot` ASC) ,
  INDEX `idx_bnt_instance` (`bnt_instance` ASC) ,
  UNIQUE INDEX `uk_bnt_instance_competition` (`bnt_instance` ASC, `bnt_competition` ASC) ,
  INDEX `fk_bnt_gnc_idx` (`bnt_genco` ASC) ,
  INDEX `fk_bnt_buy_idx` (`bnt_buyer` ASC) ,
  CONSTRAINT `fk_bnt_cmp`
    FOREIGN KEY (`bnt_competition` )
    REFERENCES `pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_bkr`
    FOREIGN KEY (`bnt_broker` )
    REFERENCES `pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_tsl`
    FOREIGN KEY (`bnt_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_gnc`
    FOREIGN KEY (`bnt_genco` )
    REFERENCES `pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_buy`
    FOREIGN KEY (`bnt_buyer` )
    REFERENCES `pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_hourly_charge`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_hourly_charge` ;

CREATE  TABLE IF NOT EXISTS `pla_hourly_charge` (
  `hcg_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `hcg_instance` INT(11) UNSIGNED NOT NULL ,
  `hcg_rate` INT(11) UNSIGNED NULL ,
  `hcg_timeslot` INT(11) UNSIGNED NOT NULL ,
  `hcg_value` DECIMAL(15,5) NULL ,
  PRIMARY KEY (`hcg_id`) ,
  INDEX `fk_hcg_rate_idx` (`hcg_rate` ASC) ,
  INDEX `fk_hcg_timeslot_idx` (`hcg_timeslot` ASC) ,
  CONSTRAINT `fk_hcg_rate`
    FOREIGN KEY (`hcg_rate` )
    REFERENCES `pla_rate` (`rte_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_hcg_timeslot`
    FOREIGN KEY (`hcg_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_balancing_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_balancing_order` ;

CREATE  TABLE IF NOT EXISTS `pla_balancing_order` (
  `bor_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bor_instance` INT(11) UNSIGNED NOT NULL ,
  `bor_tariff` INT(11) UNSIGNED NOT NULL ,
  `bor_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bor_exercise_ratio` DECIMAL(15,5) NOT NULL ,
  `bor_price` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`bor_id`) ,
  INDEX `fk_bor_tariff_idx` (`bor_tariff` ASC) ,
  INDEX `fk_bor_timeslot_idx` (`bor_timeslot` ASC) ,
  CONSTRAINT `fk_bor_tariff`
    FOREIGN KEY (`bor_tariff` )
    REFERENCES `pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bor_timeslot`
    FOREIGN KEY (`bor_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;


-- -----------------------------------------------------
-- Table `pla_balancing_control_event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla_balancing_control_event` ;

CREATE  TABLE IF NOT EXISTS `pla_balancing_control_event` (
  `bce_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bce_instance` INT(11) UNSIGNED NOT NULL ,
  `bce_tariff` INT(11) UNSIGNED NOT NULL ,
  `bce_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bce_kwh` DECIMAL(15,5) NOT NULL ,
  `bce_payment` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`bce_id`) ,
  INDEX `fk_bce_timeslot_idx` (`bce_timeslot` ASC) ,
  INDEX `fk_bce_tariff_idx` (`bce_tariff` ASC) ,
  CONSTRAINT `fk_bce_timeslot`
    FOREIGN KEY (`bce_timeslot` )
    REFERENCES `pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bce_tariff`
    FOREIGN KEY (`bce_tariff` )
    REFERENCES `pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = INNODB;





DROP procedure IF EXISTS `import`;

DELIMITER $$

CREATE PROCEDURE `import`(comment VARCHAR(256), mark_processed BOOLEAN, format VARCHAR(16))
import_proc_label:BEGIN
    # --- Store new competition ---
    DECLARE cmpname VARCHAR(256);
    DECLARE cmpid INT;

	SET FOREIGN_KEY_CHECKS = 0;

	IF format = '2012' THEN
		CALL do_log(concat_ws(',','Importing using format 2012',cmpid,cmpname));
	ELSEIF format = '2013' THEN
		CALL do_log(concat_ws(',','Importing using format 2013',cmpid,cmpname));
	ELSE
		CALL do_log(concat_ws(',','Unknown format -',format,'- aborting import.',cmpid,cmpname));
		LEAVE import_proc_label;
	END IF;

    SELECT imp_arg1
        INTO cmpname
        FROM pla_import
        WHERE imp_class='org.powertac.common.Competition' 
        AND imp_method='new';
  
    IF cmpname IS NULL THEN
        # There was no competition because it was already processed:
        # use the highest available competition id
        SELECT max(cmp_id)
            INTO cmpid
            FROM pla_competition;
    ELSE
        INSERT IGNORE INTO pla_competition (cmp_name) VALUES (cmpname);
        SET cmpid = LAST_INSERT_ID();
    END IF;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Competition', 'new');
    END IF;

    CALL do_log(concat_ws(',','Importing competition',cmpid,cmpname));

    -- ----------------------------------------------------
    -- Store competition properties
    -- ----------------------------------------------------
    CALL do_log(concat_ws(',','Storing competition properties for competition',cmpid,cmpname));

    INSERT IGNORE INTO pla_competition_properties
        (cpr_name,cpr_value,cpr_competition)
    (SELECT substr(imp_method,5), imp_arg1, cmpid
        FROM pla_import
        WHERE imp_class='org.powertac.common.Competition' 
        AND imp_method LIKE 'with%');
  
    # Mark as processed
    IF mark_processed THEN
        UPDATE pla_import
        SET imp_target = cmpid
        WHERE imp_class='org.powertac.common.Competition' 
        AND imp_method LIKE 'with%';
    END IF;

    INSERT IGNORE INTO pla_competition_properties
        (cpr_name,cpr_value,cpr_competition)
    (SELECT 'SimStart', imp_arg1, cmpid
        FROM pla_import
        WHERE imp_class='org.powertac.common.msg.SimStart');

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.SimStart', 'new');
    END IF;

    IF comment IS NOT NULL THEN
        INSERT IGNORE INTO pla_competition_properties
            (cpr_name,cpr_value,cpr_competition)
        VALUES
            ('Comment',comment,cmpid);
    END IF;

    -- ----------------------------------------------------
    -- Store customers
    -- ----------------------------------------------------

    CALL import_customers(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store brokers
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing brokers for competition',cmpid,cmpname));

    INSERT IGNORE INTO pla_broker
        (bkr_instance,bkr_competition,bkr_name)
    (SELECT imp_instance_id,cmpid,imp_arg1
        FROM pla_import
        WHERE (   imp_class='org.powertac.common.Broker' OR 
                  imp_class='org.powertac.du.DefaultBroker')
        AND imp_method='new');

    IF mark_processed THEN
        UPDATE pla_import
        SET imp_target = cmpid
        WHERE ( imp_class='org.powertac.common.Broker' OR 
                imp_class='org.powertac.du.DefaultBroker') AND 
                imp_method = 'new';
    END IF;

    -- ----------------------------------------------------
    -- Store timeslots
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing original timeslots for competition',cmpid,cmpname));

    INSERT IGNORE INTO pla_timeslot
        (tsl_competition,tsl_serial,tsl_time,tsl_timestring)
    (SELECT DISTINCT cmpid,convert_key(imp_arg2)-1,convert_timestamp(imp_arg1),imp_arg1
        FROM pla_import
        WHERE imp_class='org.powertac.common.msg.TimeslotUpdate'
        AND imp_method='new');

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));
	CALL do_log(concat_ws(',','Storing past-game timeslots for competition',cmpid,cmpname));

	INSERT IGNORE INTO pla_timeslot
        (tsl_competition,tsl_serial,tsl_time,tsl_timestring)
	(SELECT 
		cmpid, new_serial, new_time, DATE_FORMAT(new_time, '%Y-%m-%dT%H:%i:%S.000Z') new_timestring
	FROM
	(SELECT convert_key(imp_arg3) new_serial, 
			maxt.tsl_time + INTERVAL (convert_key(imp_arg3)-maxt.tsl_serial) HOUR new_time,
			maxt.* 
	FROM pla_import,
		 (SELECT *
		 FROM pla_timeslot
		 WHERE tsl_serial = 
			(SELECT MAX(tsl_serial) 
			 FROM pla_timeslot
			 WHERE tsl_competition=cmpid)
			AND tsl_competition=cmpid) maxt
	WHERE imp_class='org.powertac.common.msg.TimeslotUpdate'
		AND imp_method='new'
		AND convert_key(imp_arg3) > 
			(SELECT MAX(tsl_serial) FROM pla_timeslot WHERE tsl_competition=cmpid)) newserials);

	CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.TimeslotUpdate', 'new');
    END IF;

	IF format = '2012' THEN
		CALL do_log(concat_ws(',','Creating temporary timeslot instance mapping for competition',cmpid,cmpname));
		-- The 2012 format requires the timeslot instance IDs in some places, where
		-- later formats work exclusively with timeslot serials. Create a temporary
		-- in-memory mapping table
		CREATE TABLE IF NOT EXISTS tmp_tsl_instance_map
			(tslmap_serial INT(11) UNSIGNED,
			 tslmap_instance INT(11) UNSIGNED,
			 UNIQUE INDEX uk_tslmap_serial (tslmap_serial ASC),
			 UNIQUE INDEX uk_tslmap_instance (tslmap_instance ASC))
			engine = memory;

		TRUNCATE TABLE tmp_tsl_instance_map;

		INSERT IGNORE INTO tmp_tsl_instance_map
			(tslmap_serial,tslmap_instance)
		(SELECT convert_key(imp_arg1), imp_instance_id
			FROM pla_import
			WHERE imp_class='org.powertac.common.Timeslot'
			AND imp_method='new');

		IF mark_processed THEN
			CALL mark_as_processed(cmpid, 'org.powertac.common.Timeslot', 'new');
		END IF;

		CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));
	END IF;

    -- ----------------------------------------------------
    -- Store GenCos
    -- ----------------------------------------------------

    CALL import_gencos(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store Buyers
    -- ----------------------------------------------------

    CALL import_buyers(cmpid, mark_processed);


    -- ----------------------------------------------------
    -- Store Hydrogen Station
    -- ----------------------------------------------------

    CALL import_hydrogen_station(cmpid, mark_processed);
    -- ----------------------------------------------------
    -- Store Tariffs
    -- ----------------------------------------------------

    CALL import_tariffs(cmpid, mark_processed, format);

    -- ----------------------------------------------------
    -- Store weather forecasts
    -- ----------------------------------------------------

    CALL import_weather(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store orderbooks and their entries
    -- ----------------------------------------------------

    CALL import_orderbooks(cmpid, mark_processed, format);

    -- ----------------------------------------------------
    -- Store market transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing market transactions for competition',cmpid,cmpname));

	IF format = '2012' THEN
		INSERT IGNORE INTO pla_market_transaction
			(mtr_instance,mtr_competition,mtr_delivery_timeslot,mtr_order_timeslot,mtr_mwh,mtr_price,mtr_broker,mtr_genco,mtr_buyer)
		SELECT imp_instance_id,cmpid,delivery_timeslot.tsl_id, order_timeslot.tsl_id,imp_mwh,imp_price, bkr_id,gnc_id,buy_id
		FROM
			(SELECT imp_instance_id,
					imp_arg2 imp_tsl_order_timestring,
					convert_number(imp_arg4) imp_mwh,
					convert_number(imp_arg5) imp_price,
					convert_key(imp_arg3) imp_tsl_delivery_instance,
					convert_key(imp_arg1) imp_entity_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.MarketTransaction' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON (tslmap_instance = imp_tsl_delivery_instance)
		LEFT JOIN pla_timeslot delivery_timeslot
			ON (delivery_timeslot.tsl_serial = tslmap_serial AND delivery_timeslot.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot order_timeslot
			ON (order_timeslot.tsl_timestring = imp_tsl_order_timestring AND order_timeslot.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker bkr
			ON (bkr.bkr_instance = imp_entity_instance AND bkr.bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco gnc
			ON (gnc.gnc_instance = imp_entity_instance AND gnc.gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer buy
			ON (buy.buy_instance = imp_entity_instance AND buy.buy_competition = cmpid);
	ELSE
		INSERT IGNORE INTO pla_market_transaction
			(mtr_instance,mtr_competition,mtr_delivery_timeslot,mtr_order_timeslot,mtr_mwh,mtr_price,mtr_broker,mtr_genco,mtr_buyer)
		SELECT imp_instance_id,cmpid,delivery_timeslot.tsl_id, order_timeslot.tsl_id,imp_mwh,imp_price, bkr_id,gnc_id,buy_id
		FROM
			(SELECT imp_instance_id,
					convert_key(imp_arg2) imp_tsl_order_serial,
					convert_number(imp_arg4) imp_mwh,
					convert_number(imp_arg5) imp_price,
					convert_key(imp_arg3) imp_tsl_delivery_serial,
					convert_key(imp_arg1) imp_entity_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.MarketTransaction' AND imp_method='new') imp
		LEFT JOIN pla_timeslot delivery_timeslot
			ON (delivery_timeslot.tsl_serial = imp_tsl_delivery_serial AND delivery_timeslot.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot order_timeslot
			ON (order_timeslot.tsl_serial = imp_tsl_order_serial AND order_timeslot.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker bkr
			ON (bkr.bkr_instance = imp_entity_instance AND bkr.bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco gnc
			ON (gnc.gnc_instance = imp_entity_instance AND gnc.gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer buy
			ON (buy.buy_instance = imp_entity_instance AND buy.buy_competition = cmpid);
	END IF;
    
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.MarketTransaction', 'new');
    END IF;


    -- ----------------------------------------------------
    -- Store cash positions and their changes
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing cash positions for competition',cmpid,cmpname));

    INSERT IGNORE INTO pla_cash_position
        (cpo_instance,cpo_competition,cpo_broker,cpo_genco,cpo_buyer,cpo_balance,cpo_timeslot)
	SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,balance,tsl_id    
FROM
(SELECT imp_instance_id,cmpid,convert_number(imp_arg2) as balance, imp_arg3, imp_arg1
     FROM pla_import 
     WHERE imp_class='org.powertac.common.CashPosition' AND imp_method='new') imp
	 LEFT JOIN pla_timeslot
	   ON (tsl_serial=convert_key(imp_arg3) AND tsl_competition=cmpid)
     LEFT JOIN pla_broker
       ON (bkr_instance=convert_key(imp_arg1) AND bkr_competition=cmpid)
     LEFT JOIN pla_genco
       ON (gnc_instance=convert_key(imp_arg1) AND gnc_competition=cmpid)
     LEFT JOIN pla_buyer
       ON (buy_instance=convert_key(imp_arg1) AND buy_competition=cmpid);
  
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CashPosition', 'new');
    END IF;

  -- ----------------------------------------------------
  -- Store market positions and their changes. 
  -- ----------------------------------------------------

    CALL import_market_positions(cmpid, mark_processed, format);

  -- ----------------------------------------------------
  -- Store Orders
  -- ----------------------------------------------------  

    CALL do_log(concat_ws(',','Storing orders for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT IGNORE INTO pla_order
			(ord_instance,ord_competition,ord_broker,ord_genco,ord_buyer,ord_timeslot,ord_timeslot_placed,ord_mwh,ord_limit)     
		SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,timeslot_order.tsl_id,timeslot_placed.tsl_id,convert_number(imp_arg3) imp_mwh,convert_number(imp_arg4) imp_limit
		FROM
		(SELECT imp_instance_id,
				imp_ts,
				imp_arg3,
				imp_arg4,
				convert_key(imp_arg2) imp_tsl_instance,
				convert_key(imp_arg1) imp_entity_instance
		FROM pla_import
		WHERE imp_class='org.powertac.common.Order' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON imp_tsl_instance = tslmap_instance
		LEFT JOIN pla_timeslot timeslot_order
			ON (timeslot_order.tsl_serial = tslmap_serial AND timeslot_order.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot timeslot_placed
			ON (timeslot_placed.tsl_serial = imp_ts AND timeslot_placed.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker
			ON (bkr_instance = imp_entity_instance AND bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco
			ON (gnc_instance = imp_entity_instance AND gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer
			ON (buy_instance = imp_entity_instance AND buy_competition = cmpid)
		-- Guard against erroneous orders for timeslots outside the actual game time
		WHERE timeslot_order.tsl_id IS NOT NULL;
	ELSE
		INSERT IGNORE INTO pla_order
			(ord_instance,ord_competition,ord_broker,ord_genco,ord_buyer,ord_timeslot,ord_timeslot_placed,ord_mwh,ord_limit)     
		SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,timeslot_order.tsl_id,timeslot_placed.tsl_id,convert_number(imp_arg3) imp_mwh,convert_number(imp_arg4) imp_limit
		FROM
		(SELECT imp_instance_id,
				imp_ts,
				imp_arg3,
				imp_arg4,
				convert_key(imp_arg2) imp_tsl_serial,
				convert_key(imp_arg1) imp_entity_instance
		FROM pla_import
		WHERE imp_class='org.powertac.common.Order' AND imp_method='new') imp
		LEFT JOIN pla_timeslot timeslot_order
			ON (timeslot_order.tsl_serial = imp_tsl_serial AND timeslot_order.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot timeslot_placed
			ON (timeslot_placed.tsl_serial = imp_ts AND timeslot_placed.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker
			ON (bkr_instance = imp_entity_instance AND bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco
			ON (gnc_instance = imp_entity_instance AND gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer
			ON (buy_instance = imp_entity_instance AND buy_competition = cmpid)
		-- Guard against erroneous orders for timeslots outside the actual game time
		WHERE timeslot_order.tsl_id IS NOT NULL; 
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Order', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store Cleared Trades
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing cleared trades for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT IGNORE INTO pla_cleared_trade
			(clt_instance,clt_competition,clt_timeslot,clt_execution_mwh,clt_execution_price,clt_date_executed)
		SELECT imp_instance_id,cmpid,tsl_id,imp_execution_mwh,imp_execution_price,imp_date_executed
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg2) imp_execution_mwh,
					convert_number(imp_arg3) imp_execution_price,
					convert_timestamp(imp_arg4) imp_date_executed,
					convert_key(imp_arg1) imp_tsl_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.ClearedTrade' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON imp_tsl_instance = tslmap_instance
		LEFT JOIN pla_timeslot
			ON (tsl_serial=tslmap_serial AND tsl_competition=cmpid);
	ELSE
		INSERT IGNORE INTO pla_cleared_trade
			(clt_instance,clt_competition,clt_timeslot,clt_execution_mwh,clt_execution_price,clt_date_executed)
		SELECT imp_instance_id,cmpid,tsl_id,imp_execution_mwh,imp_execution_price,imp_date_executed
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg2) imp_execution_mwh,
					convert_number(imp_arg3) imp_execution_price,
					convert_timestamp(imp_arg4) imp_date_executed,
					convert_key(imp_arg1) imp_tsl_serial
			FROM pla_import
			WHERE imp_class='org.powertac.common.ClearedTrade' AND imp_method='new') imp
		LEFT JOIN pla_timeslot
			ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.ClearedTrade', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store balancing transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing balancing transactions for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT IGNORE INTO pla_balancing_transaction
			(btr_instance,btr_competition,btr_broker,btr_timeslot,btr_kwh,btr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_btr_kwh,imp_btr_charge
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg3) imp_btr_kwh,
					convert_number(imp_arg4) imp_btr_charge,
					imp_arg2 imp_tsl_timestring,
					convert_key(imp_arg1) imp_bkr_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.BalancingTransaction' AND imp_method='new') imp
		LEFT JOIN pla_timeslot 
			ON (tsl_timestring=imp_tsl_timestring AND tsl_competition=cmpid)
		LEFT JOIN pla_broker 
			ON (bkr_instance=imp_bkr_instance AND bkr_competition=cmpid);
	ELSE
		INSERT IGNORE INTO pla_balancing_transaction
			(btr_instance,btr_competition,btr_broker,btr_timeslot,btr_kwh,btr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_btr_kwh,imp_btr_charge
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg3) imp_btr_kwh,
					convert_number(imp_arg4) imp_btr_charge,
					convert_key(imp_arg2) imp_tsl_serial,
					convert_key(imp_arg1) imp_bkr_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.BalancingTransaction' AND imp_method='new') imp
		LEFT JOIN pla_timeslot 
			ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid)
		LEFT JOIN pla_broker 
			ON (bkr_instance=imp_bkr_instance AND bkr_competition=cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.BalancingTransaction', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store distribution transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing distribution transactions for competition',cmpid,cmpname));

	IF FORMAT = "2012" THEN
		INSERT IGNORE INTO pla_distribution_transaction
			(dtr_instance,dtr_competition,dtr_broker,dtr_timeslot,dtr_kwh,dtr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_dtr_kwh,imp_dtr_charge
		FROM
			(SELECT 
				imp_instance_id,
				convert_number(imp_arg3) imp_dtr_kwh,
				convert_number(imp_arg4) imp_dtr_charge,
				imp_arg2 imp_tsl_timestring,
				convert_key(imp_arg1) imp_bkr_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.DistributionTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON tsl_timestring=imp_tsl_timestring AND tsl_competition=cmpid
			LEFT JOIN pla_broker 
				ON bkr_instance=imp_bkr_instance AND bkr_competition=cmpid;
	ELSE
		INSERT IGNORE INTO pla_distribution_transaction
			(dtr_instance,dtr_competition,dtr_broker,dtr_timeslot,dtr_kwh,dtr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_dtr_kwh,imp_dtr_charge
		FROM
			(SELECT 
				imp_instance_id,
				convert_number(imp_arg3) imp_dtr_kwh,
				convert_number(imp_arg4) imp_dtr_charge,
				convert_key(imp_arg2) imp_tsl_serial,
				convert_key(imp_arg1) imp_bkr_instance
			FROM pla_import
			WHERE imp_class='org.powertac.common.DistributionTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON tsl_serial=imp_tsl_serial AND tsl_competition=cmpid
			LEFT JOIN pla_broker 
				ON bkr_instance=imp_bkr_instance AND bkr_competition=cmpid;
	END IF;
	
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.DistributionTransaction', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store bank transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing bank transactions for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT IGNORE INTO pla_bank_transaction
			(bnt_instance,bnt_competition,bnt_broker,bnt_genco,bnt_buyer,bnt_timeslot,bnt_amount)
		   SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,tsl_id,imp_bnt_amount
			FROM
				(SELECT 
					imp_instance_id,
					convert_number(imp_arg2) imp_bnt_amount,
					imp_arg3 imp_tsl_timestring,
					convert_key(imp_arg1) imp_entity_instance
				FROM pla_import
				WHERE imp_class='org.powertac.common.BankTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON (tsl_timestring=imp_tsl_timestring AND tsl_competition=cmpid)
			LEFT JOIN pla_broker 
				ON (bkr_instance=imp_entity_instance AND bkr_competition=cmpid)
			LEFT JOIN pla_genco
				ON (gnc_instance=imp_entity_instance AND gnc_competition=cmpid)
			LEFT JOIN pla_buyer 
				ON (buy_instance=imp_entity_instance AND buy_competition=cmpid);
	ELSE
		INSERT IGNORE INTO pla_bank_transaction
			(bnt_instance,bnt_competition,bnt_broker,bnt_genco,bnt_buyer,bnt_timeslot,bnt_amount)
		   SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,tsl_id,imp_bnt_amount
			FROM
				(SELECT 
					imp_instance_id,
					convert_number(imp_arg2) imp_bnt_amount,
					convert_key(imp_arg3) imp_tsl_serial,
					convert_key(imp_arg1) imp_entity_instance
				FROM pla_import
				WHERE imp_class='org.powertac.common.BankTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid)
			LEFT JOIN pla_broker 
				ON (bkr_instance=imp_entity_instance AND bkr_competition=cmpid)
			LEFT JOIN pla_genco
				ON (gnc_instance=imp_entity_instance AND gnc_competition=cmpid)
			LEFT JOIN pla_buyer 
				ON (buy_instance=imp_entity_instance AND buy_competition=cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.BankTransaction', 'new');
    END IF;

  -- ----------------------------------------------------
  -- Store balancing orders and control events
  -- ----------------------------------------------------


    CALL do_log(concat_ws(',','Storing balancing orders for competition',cmpid,cmpname));

    INSERT IGNORE INTO pla_balancing_order
        (bor_instance,bor_tariff,bor_timeslot,bor_exercise_ratio,bor_price)
       SELECT imp_instance_id,trf_id,tsl_id,bor_exercise_ratio,bor_price
        FROM
            (SELECT 
                imp_instance_id,
                imp_ts,
                convert_number(imp_arg1) bor_exercise_ratio,
                convert_number(imp_arg2) bor_price,
                convert_key(imp_arg3) imp_tariff_instance
            FROM pla_import
            WHERE imp_class='org.powertac.common.msg.BalancingOrder' AND imp_method='new') imp
        LEFT JOIN pla_tariff
            ON (trf_instance=imp_tariff_instance AND trf_competition=cmpid)
        LEFT JOIN pla_timeslot 
            ON (tsl_serial=imp_ts AND tsl_competition=cmpid);
    
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.BalancingOrder', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing balancing control events for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT IGNORE INTO pla_balancing_control_event
			(bce_instance,bce_tariff,bce_timeslot,bce_kwh,bce_payment)
		   SELECT imp_instance_id,trf_id,tsl_id,bce_kwh,bce_payment
			FROM
				(SELECT 
					imp_instance_id,
					convert_key(imp_arg1) imp_tariff_instance,
					convert_number(imp_arg2) bce_kwh,
					convert_number(imp_arg3) bce_payment,
					convert_key(imp_arg4) imp_tsl_serial
				FROM pla_import
				WHERE imp_class='org.powertac.common.msg.BalancingControlEvent' AND imp_method='new') imp
			LEFT JOIN pla_tariff
				ON (trf_instance=imp_tariff_instance AND trf_competition=cmpid)
			LEFT JOIN pla_timeslot 
				ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid);
	ELSE
		INSERT IGNORE INTO pla_balancing_control_event
			(bce_instance,bce_tariff,bce_timeslot,bce_kwh,bce_payment)
		   SELECT imp_instance_id,trf_id,tsl_id,bce_kwh,bce_payment
			FROM
				(SELECT 
					imp_instance_id,
					convert_key(imp_arg1) imp_tariff_instance,
					convert_number(imp_arg2) bce_kwh,
					convert_number(imp_arg3) bce_payment,
					convert_key(imp_arg4) imp_tsl_serial
				FROM pla_import
				WHERE imp_class='org.powertac.common.msg.BalancingControlEvent' AND imp_method='new') imp
			LEFT JOIN pla_tariff
				ON (trf_instance=imp_tariff_instance AND trf_competition=cmpid)
			LEFT JOIN pla_timeslot 
				ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid);
    END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.BalancingControlEvent', 'new');
    END IF;
  -- ----------------------------------------------------
  -- Mark simulation end
  -- ----------------------------------------------------

    INSERT IGNORE INTO pla_competition_properties
        (cpr_name,cpr_value,cpr_competition)
    (SELECT 'SimEnd',imp_ts, cmpid
    FROM pla_import
    WHERE imp_class='org.powertac.common.msg.SimEnd');
    
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.SimEnd', 'new');
    END IF;

	IF format = '2012' THEN
		DROP TABLE IF EXISTS tmp_tsl_instance_map;
	END IF;

	SET FOREIGN_KEY_CHECKS = 1;
    CALL do_log(concat_ws(',','Done'));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure setup_log
-- -----------------------------------------------------


DROP procedure IF EXISTS `setup_log`;

DELIMITER $$



CREATE PROCEDURE `setup_log` ()
BEGIN
    CREATE TABLE IF NOT EXISTS pla_log 
        (time DATETIME,
         msg varchar(512)) 
    engine = memory;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure do_log
-- -----------------------------------------------------


DROP procedure IF EXISTS `do_log`;

DELIMITER $$



CREATE PROCEDURE `do_log` (in logMsg VARCHAR(512))
BEGIN
Declare continue handler for 1146 -- Table not found
  BEGIN
     call setup_log();
     INSERT IGNORE into pla_log values(CURRENT_TIMESTAMP(),logMsg);
  END;
 
  INSERT IGNORE into pla_log values(CURRENT_TIMESTAMP(),logMsg);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function convert_timestamp
-- -----------------------------------------------------


DROP function IF EXISTS `convert_timestamp`;

DELIMITER $$



CREATE FUNCTION `convert_timestamp` (timestamp_string VARCHAR(256)) RETURNS DATETIME
BEGIN
    DECLARE conv_string VARCHAR(256);

    IF timestamp_string = "null" THEN
        RETURN NULL;
    ELSE
        SET conv_string = replace(substr(timestamp_string FROM 1 FOR length(timestamp_string) - 5), 'T',' ');
        return(CAST(conv_string AS DATETIME));
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure split_string
-- -----------------------------------------------------


DROP procedure IF EXISTS `split_string`;

DELIMITER $$



CREATE PROCEDURE `split_string` (input TEXT, tmp_table VARCHAR(255))
BEGIN
    DECLARE cur_position INT DEFAULT 1 ;
    DECLARE remainder TEXT;
    DECLARE cur_string VARCHAR(1000);
    DECLARE delimiter_length TINYINT UNSIGNED;
    DECLARE delimiter CHAR(1);

    SET @query_drop = CONCAT('DROP TEMPORARY TABLE IF EXISTS ', tmp_table);
    PREPARE smt_drop FROM @query_drop;
    EXECUTE smt_drop;

    SET @query_create = CONCAT('CREATE TEMPORARY TABLE ', tmp_table, '(id VARCHAR(11) NOT NULL) ENGINE=INNODB;');
    PREPARE stm_create FROM @query_create;
    EXECUTE stm_create;

    SET @query_insert = CONCAT('INSERT IGNORE INTO ', tmp_table, ' VALUES (?);');
    PREPARE stm_insert FROM @query_insert;

    SET delimiter = ',';
    SET remainder = input;
    SET delimiter_length = CHAR_LENGTH(delimiter);

    WHILE CHAR_LENGTH(remainder) > 0 AND cur_position > 0 DO
        SET cur_position = INSTR(remainder, `delimiter`);
        IF cur_position = 0 THEN
            SET cur_string = remainder;
        ELSE
            SET cur_string = LEFT(remainder, cur_position - 1);
        END IF;
        IF TRIM(cur_string) != '' THEN
            SET @id = cur_string;
            EXECUTE stm_insert USING @id;
        END IF;
        SET remainder = SUBSTRING(remainder, cur_position + delimiter_length);
    END WHILE;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_weather
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_weather`;

DELIMITER $$

CREATE PROCEDURE `import_weather` (cmpid INT, mark_processed BOOLEAN)
BEGIN
  
    
CALL do_log(concat_ws(',','Adding forecast',cmpid));


INSERT IGNORE INTO pla_weather_forecast
        (wfc_instance,wfc_competition,wfc_timeslot,wfc_data)
    SELECT imp_instance_id, cmpid, tsl_id, imp_arg2
    FROM
       (SELECT imp_instance_id,
           tsl_id,
           imp_arg2
    FROM pla_import,pla_timeslot
    WHERE imp_class='org.powertac.common.WeatherForecast' 
    AND imp_method='new'
    AND tsl_serial=convert_key(imp_arg1) AND tsl_competition=cmpid) imp;


CALL do_log(concat_ws(',','Adding children to forecast',cmpid));

INSERT IGNORE INTO pla_weather_forecast_data
    (wfd_instance,wfd_competition,wfd_offset,wfd_temperature,wfd_wind_speed,wfd_wind_direction,wfd_cloud_cover)
       (SELECT imp_instance_id,
		cmpid as wfd_competition,
               convert_key(imp_arg1),
               convert_number(imp_arg2),
               convert_number(imp_arg3),
               convert_number(imp_arg4),
               convert_number(imp_arg5)
        FROM pla_import
        WHERE imp_class='org.powertac.common.WeatherForecastPrediction' 
        AND imp_method='new');


CALL do_log(concat_ws(',','Importing weather reports for competition',cmpid));



    INSERT IGNORE INTO pla_weather_report
        (wre_instance,wre_competition,wre_timeslot,wre_temperature,wre_wind_speed,wre_wind_direction,wre_cloud_cover)
    SELECT imp_instance_id, cmpid, tsl_id, imp_temperature, imp_wind_speed, imp_wind_direction, imp_cloud_cover
    FROM
       (SELECT imp_instance_id,
               convert_number(imp_arg2) imp_temperature,
               convert_number(imp_arg3) imp_wind_speed,
               convert_number(imp_arg4) imp_wind_direction,
               convert_number(imp_arg5) imp_cloud_cover,
               convert_key(imp_arg1) imp_tsl_serial
        FROM pla_import
        WHERE imp_class='org.powertac.common.WeatherReport' 
        AND imp_method='new') imp
    INNER JOIN pla_timeslot
        ON tsl_serial=imp_tsl_serial AND tsl_competition=cmpid;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid,'org.powertac.common.WeatherReport','new');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure mark_as_processed
-- -----------------------------------------------------


DROP procedure IF EXISTS `mark_as_processed`;

DELIMITER $$



CREATE PROCEDURE `mark_as_processed` (cmpid INT, class VARCHAR(256), method VARCHAR(256))
BEGIN

  CALL do_log(concat_ws(',','Marking as read',cmpid,class,method));
  UPDATE pla_import
    SET imp_target = cmpid
    WHERE imp_class = class AND imp_method = method AND imp_target IS NULL;
  CALL do_log(concat_ws(',','Import rows marked',ROW_COUNT()));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_orderbooks
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_orderbooks`;

DELIMITER $$

CREATE PROCEDURE `import_orderbooks` (cmpid INT, mark_processed BOOLEAN, format VARCHAR(16))
BEGIN

    CALL do_log(concat_ws(',','Importing orderbooks for competition',cmpid));

	IF format = '2012' THEN
		INSERT IGNORE INTO pla_orderbook
			(obk_instance,obk_competition,obk_timeslot,obk_clearing_price,obk_date_executed)
		SELECT imp_instance_id, cmpid, tsl_id, obk_clearing_price, obk_date_executed 
		FROM
			(SELECT imp_instance_id,
				convert_number(imp_arg2) obk_clearing_price, 
				convert_timestamp(imp_arg3) obk_date_executed,
				convert_key(imp_arg1) imp_tsl_instance 
			FROM pla_import
			WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='new') imp,
		tmp_tsl_instance_map, pla_timeslot
		WHERE
			tslmap_instance = imp_tsl_instance AND
			tslmap_serial = tsl_serial AND 
			tsl_competition = cmpid;
	ELSE
		INSERT IGNORE INTO pla_orderbook
			(obk_instance,obk_competition,obk_timeslot,obk_clearing_price,obk_date_executed)
		SELECT imp_instance_id, cmpid, tsl_id, obk_clearing_price, obk_date_executed FROM
		(SELECT imp_instance_id,
			convert_number(imp_arg2) obk_clearing_price, 
			convert_timestamp(imp_arg3) obk_date_executed,
			convert_key(imp_arg1) imp_tsl_serial 
		FROM pla_import
		WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='new') imp
		LEFT JOIN pla_timeslot
			ON (tsl_serial = imp_tsl_serial AND tsl_competition = cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Orderbook', 'new');
    END IF;

    # We first create these entries without reference to their orderbooks. These
    # dangling references are then fixed below.
    CALL do_log(concat_ws(',','Importing orderbook orders for competition',cmpid));

    INSERT IGNORE INTO pla_orderbook_order
        (obo_instance,obo_competition,obo_mwh,obo_limit_price)
    (SELECT imp_instance_id,cmpid,
        convert_number(imp_arg1),
        convert_number(imp_arg2) 
    FROM pla_import
    WHERE imp_class='org.powertac.common.OrderbookOrder' AND imp_method='new');

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.OrderbookOrder', 'new');
    END IF;

    CALL do_log(concat_ws(',','Updating orderbook bids for competition',cmpid));

    UPDATE pla_orderbook_order,
        (SELECT obo_id, obk_id FROM
            (SELECT imp_instance_id imp_obk_instance, convert_key(imp_arg1) imp_obo_instance
            FROM pla_import 
            WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='addBid') imp
        LEFT JOIN pla_orderbook
            ON (imp_obk_instance = obk_instance AND obk_competition = cmpid)
        LEFT JOIN pla_orderbook_order
            ON (imp_obo_instance = obo_instance AND obo_competition = cmpid)) jointbl
    SET obo_orderbook = jointbl.obk_id, obo_type = 'BID'
    WHERE pla_orderbook_order.obo_id = jointbl.obo_id;
        
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Orderbook', 'addBid');
    END IF;

    CALL do_log(concat_ws(',','Updating orderbook asks for competition',cmpid));

    UPDATE pla_orderbook_order,
    (SELECT obo_id, obk_id FROM
        (SELECT imp_instance_id imp_obk_instance, convert_key(imp_arg1) imp_obo_instance
        FROM pla_import 
        WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='addAsk') imp
    LEFT JOIN pla_orderbook
        ON (imp_obk_instance = obk_instance AND obk_competition = cmpid)
    LEFT JOIN pla_orderbook_order
        ON (imp_obo_instance = obo_instance AND obo_competition = cmpid)) jointbl
    SET obo_orderbook = jointbl.obk_id, obo_type = 'ASK'
    WHERE pla_orderbook_order.obo_id = jointbl.obo_id;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Orderbook', 'addAsk');
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_gencos
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_gencos`;

DELIMITER $$



CREATE PROCEDURE `import_gencos` (cmpid INT, mark_processed BOOLEAN)
BEGIN

    CALL do_log(concat_ws(',','Storing gencos for competition',cmpid));
    INSERT IGNORE INTO pla_genco
        (gnc_instance,gnc_competition,gnc_name)
    (SELECT imp_instance_id,cmpid,imp_arg1
     FROM pla_import
     WHERE (imp_class='org.powertac.genco.Genco' 
            AND imp_method='new'
            -- Call to three argument constructor triggers call to default constructor
            AND imp_arg2 IS NOT NULL));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing genco nominal capacity for competition',cmpid));
    UPDATE pla_genco,pla_import
    SET gnc_nominal_capacity = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withNominalCapacity'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withNominalCapacity');
    END IF;

    CALL do_log(concat_ws(',','Storing genco variability for competition',cmpid));
    
    UPDATE pla_genco,pla_import
    SET gnc_variability = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withVariability'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withVariability');
    END IF;

    CALL do_log(concat_ws(',','Storing genco reliability for competition',cmpid));
    
    UPDATE pla_genco,pla_import
    SET gnc_reliability = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withReliability'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withReliability');
    END IF;

    CALL do_log(concat_ws(',','Storing genco cost for competition',cmpid));

    UPDATE pla_genco,pla_import
    SET gnc_cost = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withCost'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withCost');
    END IF;

    CALL do_log(concat_ws(',','Storing genco carbon emission rate for competition',cmpid));

    UPDATE pla_genco,pla_import
    SET gnc_carbon_emission_rate = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withCarbonEmissionRate'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withCarbonEmissionRate');
    END IF;

    CALL do_log(concat_ws(',','Storing genco commitment leadtime for competition',cmpid));

    UPDATE pla_genco,pla_import
    SET gnc_commitment_leadtime = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withCommitmentLeadtime'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withCommitmentLeadtime');
    END IF;

    # --- Storing genco state
    CALL do_log(concat_ws(',','Storing genco state (current capacity) for competition',cmpid));

    INSERT IGNORE INTO pla_genco_state
    (gcs_genco,gcs_current_capacity,gcs_timeslot)
       (SELECT gnc_id,convert_number(imp_arg1),tsl_id
        FROM pla_import,pla_genco,pla_timeslot
        WHERE (imp_class='org.powertac.genco.Genco' AND imp_method='setCurrentCapacity')
        AND gnc_instance=imp_instance_id and gnc_competition=cmpid
        AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'setCurrentCapacity');
    END IF;

    CALL do_log(concat_ws(',','Storing genco state (in operation) for competition',cmpid));

    INSERT IGNORE INTO pla_genco_state
    (gcs_genco,gcs_in_operation,gcs_timeslot)
       (SELECT gnc_id,IF(imp_arg1 = 'true', TRUE, FALSE),tsl_id
        FROM pla_import,pla_genco,pla_timeslot
        WHERE (imp_class='org.powertac.genco.Genco' AND imp_method='setInOperation')
        AND gnc_instance=imp_instance_id and gnc_competition=cmpid
        AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'setInOperation');
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function convert_number
-- -----------------------------------------------------


DROP function IF EXISTS `convert_number`;

DELIMITER $$



CREATE FUNCTION `convert_number`(instring VARCHAR(256)) 
RETURNS DECIMAL(15,5)
BEGIN
  DECLARE pos INTEGER;
  DECLARE epos INTEGER;
  
  DECLARE s VARCHAR(256);

  DECLARE exp INTEGER;
  DECLARE base DECIMAL(15,5);

  SET s = replace(instring, 'null', '0.0');
  SET s = replace(s, 'NaN', '0.0');
  SET s = replace(s, 'Infinity', '0.0');
  SET pos = INSTR(s, '.');
  SET epos = INSTR(s, 'E');

  IF(pos > 0 && epos <= 0) THEN
    SELECT CAST(substring(s, 1, pos + 5) AS DECIMAL(15,5)) INTO base;
    RETURN base;
  ELSEIF(pos > 0 && epos > 0) THEN
    SELECT substring(s, epos + 1, length(s)) INTO exp;

    IF exp > 9 THEN
        RETURN CAST('9999999999.99999' AS DECIMAL(15,5));
    ELSEIF exp < -9 THEN
        RETURN CAST('0000000000.00000' AS DECIMAL(15,5));
    ELSEIF epos > pos + 5 THEN
        RETURN concat(substring(s, 1, pos + 5),'E',exp);
    ELSE
        RETURN concat(substring(s, 1, epos - 1),'E',exp);
    END IF;    
  ELSE
    SELECT CAST(s AS DECIMAL(15,5)) INTO base;
  END IF;

  RETURN base;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_buyers
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_buyers`;

DELIMITER $$



CREATE PROCEDURE `import_buyers` (cmpid INT, mark_processed BOOLEAN)
BEGIN

    INSERT IGNORE INTO pla_buyer
        (buy_instance,buy_competition,buy_name)
    (SELECT imp_instance_id,cmpid,imp_arg1
    FROM pla_import
    WHERE ( imp_class='org.powertac.genco.Buyer' 
            AND imp_method='new'
            -- Call to three argument constructor triggers call to default constructor
            AND imp_arg2 IS NOT NULL));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'new');
    END IF;


    CALL do_log(concat_ws(',','Storing buyer price beta for competition',cmpid));

    UPDATE pla_buyer,pla_import
        SET buy_price_beta = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Buyer' 
      AND imp_method='setPriceBeta'
      AND buy_instance = imp_instance_id
      AND buy_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setPriceBeta');
    END IF;


    CALL do_log(concat_ws(',','Storing buyer Mwh for competition',cmpid));
    
    UPDATE pla_buyer,pla_import
    SET buy_price_beta = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Buyer' 
      AND imp_method='setMwh'
      AND buy_instance = imp_instance_id
      AND buy_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setMwh');
    END IF;

    # --- Storing buyer state
    CALL do_log(concat_ws(',','Storing buyer state (current capacity) for competition',cmpid));
    
    INSERT IGNORE INTO pla_buyer_state
        (bys_buyer,bys_current_capacity,bys_timeslot)
    (SELECT buy_id,convert_number(imp_arg1),tsl_id
     FROM pla_import,pla_buyer,pla_timeslot
     WHERE (imp_class='org.powertac.genco.Buyer' AND imp_method='setCurrentCapacity')
       AND buy_instance=imp_instance_id AND buy_competition=cmpid
       AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setCurrentCapacity');
    END IF;

    CALL do_log(concat_ws(',','Storing buyer state (in operation) for competition',cmpid));

    INSERT IGNORE INTO pla_buyer_state
        (bys_buyer,bys_in_operation,bys_timeslot)
    (SELECT buy_id,IF(imp_arg1 = 'true', TRUE, FALSE),tsl_id
    FROM pla_import,pla_buyer,pla_timeslot
    WHERE (imp_class='org.powertac.genco.Buyer' AND imp_method='setInOperation')
    AND buy_instance=imp_instance_id AND buy_competition=cmpid
    AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setInOperation');
    END IF;

END$$

DELIMITER ;


-- -----------------------------------------------------
-- procedure import_hydrogen_station
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_hydrogen_station`;

DELIMITER $$



CREATE PROCEDURE `import_hydrogen_station` (cmpid INT, mark_processed BOOLEAN)
BEGIN



    INSERT IGNORE INTO pla_hydrogen_station
        (hyd_instance, hyd_customer_instance, hyd_competition, hyd_name)
    (SELECT d1.imp_instance_id, d2.imp_instance_id, cmpid, d1.imp_arg1
    FROM pla_import d1, pla_import d2
    WHERE ( d1.imp_class='org.powertac.customer.model.HydrogenStorage' AND d2.imp_class='org.powertac.common.CustomerInfo' AND d2.imp_method='new' AND d2.imp_arg1=d1.imp_arg1 AND d1.imp_method='new'AND d1.imp_arg1 IS NOT NULL));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'new');
    END IF;


    CALL do_log(concat_ws(',','Storing hydrogen station parameters.',cmpid));

    UPDATE pla_hydrogen_station,pla_import
        SET hyd_electrolyzer_capacity = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.customer.model.HydrogenStorage' 
      AND imp_method='setElectrolyzerCapacity'
      AND hyd_instance = imp_instance_id
      AND hyd_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setElectrolyzerCapacity');
    END IF;


    UPDATE pla_hydrogen_station,pla_import
        SET hyd_hydrogen_storage_capacity = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.customer.model.HydrogenStorage' 
      AND imp_method='setCapacityKg'
      AND hyd_instance = imp_instance_id
      AND hyd_competition = cmpid;

   IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setCapacityKg');
    END IF;

    UPDATE pla_hydrogen_station,pla_import
        SET hyd_customer_population = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.customer.model.HydrogenStorage' 
      AND imp_method='setCustomerPopulation'
      AND hyd_instance = imp_instance_id
      AND hyd_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setCustomerPopulation');
    END IF;

    UPDATE pla_hydrogen_station,pla_import
        SET hyd_dss_method = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.customer.model.HydrogenStorage' 
      AND imp_method='setDssMethod'
      AND hyd_instance = imp_instance_id
      AND hyd_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setDssMethod');
    END IF;

    UPDATE pla_hydrogen_station,pla_import
        SET hyd_custom_factor = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.customer.model.HydrogenStorage' 
      AND imp_method='setCustomFactor'
      AND hyd_instance = imp_instance_id
      AND hyd_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setCustomFactor');
    END IF;

   

    # --- Storing hydrogen station state
    CALL do_log(concat_ws(',','Storing hydrogen station states for game id ',cmpid));
    
    INSERT IGNORE INTO pla_hydrogen_station_state
        (hys_hydrogen_station,hys_timeslot,hys_state_storage)
    (SELECT hyd_id,tsl_id,convert_number(imp_arg1)
     FROM pla_import,pla_hydrogen_station,pla_timeslot
     WHERE (imp_class='org.powertac.customer.model.HydrogenStorage' AND imp_method='setStateOfStorage')
       AND hyd_instance=imp_instance_id AND hyd_competition=cmpid
       AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setStateOfStorage');
    END IF;


    UPDATE pla_hydrogen_station_state,pla_import,pla_hydrogen_station,pla_timeslot
        SET hys_produced_hydrogen = convert_number(imp_arg1)
    WHERE (imp_class='org.powertac.customer.model.HydrogenStorage' AND imp_method='setProducedHydrogen')
    AND hyd_instance=imp_instance_id AND hys_hydrogen_station = hyd_id AND hyd_competition=cmpid
    AND tsl_serial=imp_ts AND hys_timeslot=tsl_id AND tsl_competition=cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setProducedHydrogen');
    END IF;



   UPDATE pla_hydrogen_station_state,pla_import,pla_hydrogen_station,pla_timeslot
        SET hys_consumed_hydrogen = convert_number(imp_arg1)
    WHERE (imp_class='org.powertac.customer.model.HydrogenStorage' AND imp_method='setConsumedHydrogen')
    AND hyd_instance=imp_instance_id AND hys_hydrogen_station = hyd_id AND hyd_competition=cmpid
    AND tsl_serial=imp_ts AND hys_timeslot=tsl_id AND tsl_competition=cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.customer.model.HydrogenStorage', 'setConsumedHydrogen');
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_tariffs
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_tariffs`;

DELIMITER $$

CREATE PROCEDURE `import_tariffs` (cmpid INT, mark_processed BOOLEAN, format VARCHAR(16))
BEGIN

    # --- Store tariffs and rates
    CALL do_log(concat_ws(',','Storing tariffs for competition',cmpid));

    INSERT IGNORE INTO pla_tariff
        (trf_instance, trf_competition, trf_broker, trf_minDuration, trf_signUpPayment, trf_earlyWithdrawPayment, trf_periodicPayment, trf_powerType)
    SELECT imp_instance_id,cmpid,bkr_id, trf_minDuration, trf_signUpPayment, trf_earlyWithdrawPayment, trf_periodicPayment, trf_powerType FROM
        (SELECT imp_instance_id,
                convert_key(imp_arg1) imp_bkr_instance, convert_number(imp_arg3) trf_minDuration, convert_number(imp_arg4) trf_signUpPayment, convert_number(imp_arg5) trf_earlyWithdrawPayment, convert_number(imp_arg6) trf_periodicPayment, imp_arg2 trf_powerType   
        FROM pla_import
        WHERE imp_class='org.powertac.common.TariffSpecification' AND imp_method='new'        
        GROUP BY imp_instance_id,imp_bkr_instance) imp
        LEFT JOIN pla_broker
            ON (bkr_instance=imp_bkr_instance AND bkr_competition=cmpid);

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.TariffSpecification', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing rates for competition',cmpid));

    # -- Rates Part 1: Rates created using the default constructor and "set/with" methods
    # --               These appear to be mainly used by the default broker
    INSERT IGNORE INTO pla_rate
        (rte_instance,rte_competition)
    (SELECT imp_instance_id,cmpid
    FROM pla_import
    WHERE imp_class='org.powertac.common.Rate' AND imp_method='new' and imp_arg1 IS NULL);

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        UPDATE pla_import
            SET imp_target = cmpid
            WHERE imp_class = 'org.powertac.common.Rate' AND imp_method = 'new' 
                    AND imp_arg1 IS NULL AND imp_target IS NULL;
    END IF;

    CALL do_log(concat_ws(',','Storing rate values for competition',cmpid));
    
    UPDATE pla_rate,pla_import
    SET rte_min_value = convert_number(imp_arg1),
        rte_max_value = convert_number(imp_arg1),
        rte_expected_mean = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.common.Rate' 
      AND imp_method='withValue'
      AND rte_instance = imp_instance_id
      AND rte_competition = cmpid;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Rate', 'withValue');
    END IF;

    CALL do_log(concat_ws(',','Storing rate parent tariffs for competition',cmpid));

    UPDATE pla_rate,pla_tariff,pla_import
    SET rte_tariff = trf_id
    WHERE imp_class='org.powertac.common.Rate' 
      AND imp_method='setTariffId'
      AND rte_instance = imp_instance_id
      AND rte_competition = cmpid
      AND trf_instance = convert_key(imp_arg1)
      AND trf_competition = cmpid;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Rate', 'setTariffId');
    END IF;

    # -- Rates Part 2: Rates created using the full constructor. Used by all other brokers.
    INSERT IGNORE INTO pla_rate
        (rte_instance,rte_competition,rte_tariff,
         rte_weekly_begin,rte_weekly_end,rte_daily_begin,rte_daily_end,
         rte_tier_threshold,rte_fixed,rte_min_value,rte_max_value,
         rte_notice_interval,rte_expected_mean,rte_max_curtailment)
    SELECT imp_instance_id,cmpid,trf_id,rte_weekly_begin,rte_weekly_end,
         rte_daily_begin,rte_daily_end,rte_tier_threshold,rte_fixed,
         rte_min_value,rte_max_value,rte_notice_interval,rte_expected_mean,
         rte_max_curtailment
    FROM
        (SELECT imp_instance_id,
            convert_key(imp_arg1) imp_trf_instance,
            convert_number(imp_arg2) rte_weekly_begin,
            convert_number(imp_arg3) rte_weekly_end,
            convert_number(imp_arg4) rte_daily_begin,
            convert_number(imp_arg5) rte_daily_end,
            convert_number(imp_arg6) rte_tier_threshold,
            IF(STRCMP(imp_arg7, 'true'), 1, 0) rte_fixed,
            convert_number(imp_arg8) rte_min_value,
            convert_number(imp_arg9) rte_max_value,
            convert_number(imp_arga) rte_notice_interval,
            convert_number(imp_argb) rte_expected_mean,
            convert_number(imp_argc) rte_max_curtailment
        FROM pla_import
        WHERE imp_class='org.powertac.common.Rate' AND imp_method='new' and imp_arg1 IS NOT NULL) imp
        LEFT JOIN pla_tariff
            ON (trf_instance = imp_trf_instance AND trf_competition = cmpid);

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        UPDATE pla_import
            SET imp_target = cmpid
            WHERE imp_class = 'org.powertac.common.Rate' AND imp_method = 'new' 
                    AND imp_arg1 IS NOT NULL AND imp_target IS NULL;
    END IF;

    # --- Store hourly charges
    CALL do_log(concat_ws(',','Storing hourly charges for competition',cmpid));
    
    # -- First insert all the hourly charges. This is very inconveniently modelled
    # -- in the state logs. On the one hand we have hourly charges with references
    # -- to their parnet rates; and on the other hand we have those without reference,
    # -- which are then followed by a VariableRateUpdate used to connect them.

    # -- So first for the complete hourly charges:
    INSERT IGNORE INTO pla_hourly_charge
        (hcg_instance,hcg_rate,hcg_timeslot,hcg_value)
    SELECT imp_instance_id,rte_id,tsl_id,hcg_value
    FROM
        (SELECT imp_instance_id,
            convert_key(imp_arg1) imp_rte_instance,
            convert_number(imp_arg2) hcg_value,
            imp_arg3 imp_tsl_timestring
         FROM pla_import
         WHERE imp_class='org.powertac.common.HourlyCharge' 
            AND imp_method='new'
            AND convert_number(imp_arg1) > -1) imp         
    LEFT JOIN pla_rate
        ON (rte_instance = imp_rte_instance AND rte_competition = cmpid)  
    -- Note the inner join on timestring; some brokers may specify
    -- validity times that are not at the hour boundaries. We ignore those
    -- here
    JOIN pla_timeslot
        ON (tsl_timestring = imp_tsl_timestring AND tsl_competition = cmpid);

    # -- And now for the incomplete hourly charges:
    INSERT IGNORE INTO pla_hourly_charge
        (hcg_instance,hcg_rate,hcg_timeslot,hcg_value)
    SELECT hcg_instance,rte_id,tsl_id,hcg_value
    FROM
        (SELECT imp_instance_id hcg_instance,
            imp_arg1 imp_hcg_rate_id,
            convert_number(imp_arg2) hcg_value,
            imp_arg3 imp_tsl_timestring
         FROM pla_import
         WHERE imp_class='org.powertac.common.HourlyCharge' 
            AND imp_method='new'
            AND convert_number(imp_arg1) = -1) imp_hcg
        -- Note the inner join on timestring; some brokers may specify
        -- validity times that are not at the hour boundaries. We ignore those
        -- here
        JOIN pla_timeslot
            ON (tsl_timestring = imp_hcg.imp_tsl_timestring AND tsl_competition = cmpid),
        (SELECT convert_key(imp_arg3) vru_hcg_instance,
            convert_key(imp_arg4) imp_rte_instance
         FROM pla_import
         WHERE imp_class='org.powertac.common.msg.VariableRateUpdate' 
            AND imp_method='new') imp_vru   
        LEFT JOIN pla_rate
            ON (rte_instance = imp_rte_instance AND rte_competition = cmpid)  
    WHERE hcg_instance = vru_hcg_instance;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.HourlyCharge', 'new');
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.VariableRateUpdate', 'new');   
    END IF;

    # --- Store tariff transactions
    CALL do_log(concat_ws(',','Storing tariff transactions for competition',cmpid));

	IF format = '2012' THEN
		-- The 2012 format works with joins on timestrings
		INSERT IGNORE INTO pla_tariff_transaction
			(tfc_instance,tfc_customer,tfc_tariff,tfc_timeslot,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type, tfc_regulation)
		SELECT imp_instance_id,cst_id,trf_id,tsl_id,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type, tfc_regulation
		FROM
			(SELECT imp_instance_id,
				convert_key(imp_arg6) tfc_customer_count,
				convert_number(imp_arg7) tfc_kwh,
				convert_number(imp_arg8) tfc_charge,
				imp_arg3 tfc_type,
				convert_key(imp_arg4) imp_trf_instance,
				imp_arg2 imp_tsl_timestring,
				convert_key(imp_arg5) imp_cst_instance,
				imp_arg9 tfc_regulation
			 FROM pla_import
			 WHERE imp_class='org.powertac.common.TariffTransaction' 
				AND imp_method='new'
				AND imp_arg3 IN ('CONSUME','PRODUCE','SIGNUP','WITHDRAW','PERIODIC','PUBLISH')) imp
		LEFT JOIN pla_tariff
			ON (trf_instance = imp_trf_instance AND trf_competition = cmpid)
		LEFT JOIN pla_timeslot
		    ON (tsl_timestring = imp_tsl_timestring AND tsl_competition = cmpid)
		LEFT JOIN pla_customer
			ON (cst_instance = imp_cst_instance AND cst_competition = cmpid);
	ELSE
		INSERT IGNORE INTO pla_tariff_transaction
			(tfc_instance,tfc_customer,tfc_tariff,tfc_timeslot,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type, tfc_regulation)
		SELECT imp_instance_id,cst_id,trf_id,tsl_id,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type, tfc_regulation
		FROM
			(SELECT imp_instance_id,
				convert_key(imp_arg6) tfc_customer_count,
				convert_number(imp_arg7) tfc_kwh,
				convert_number(imp_arg8) tfc_charge,
				imp_arg3 tfc_type,
				convert_key(imp_arg4) imp_trf_instance,
				convert_number(imp_arg2) imp_tsl_serial,
				convert_key(imp_arg5) imp_cst_instance,
				imp_arg9 tfc_regulation
			 FROM pla_import
			 WHERE imp_class='org.powertac.common.TariffTransaction' 
				AND imp_method='new') imp
		LEFT JOIN pla_tariff
			ON (trf_instance = imp_trf_instance AND trf_competition = cmpid)
		LEFT JOIN pla_timeslot
			ON (tsl_serial = imp_tsl_serial AND tsl_competition = cmpid)
		LEFT JOIN pla_customer
			ON (cst_instance = imp_cst_instance AND cst_competition = cmpid);
		-- WHERE 
			-- see Github Issue 565
			-- trf_id IS NOT NULL;
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.TariffTransaction', 'new');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_customers
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_customers`;

DELIMITER $$

CREATE PROCEDURE `import_customers` (cmpid INT, mark_processed BOOLEAN)
BEGIN
    # --- Store customer info
    CALL do_log(concat_ws(',','Storing customers for competition',cmpid));

    INSERT IGNORE INTO pla_customer
        (cst_competition,cst_instance,cst_name,cst_population)
    (SELECT cmpid,imp_instance_id,imp_arg1,CAST(imp_arg2 AS UNSIGNED)
     FROM pla_import
     WHERE imp_class='org.powertac.common.CustomerInfo' 
     AND imp_method='new');
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing customer power types',cmpid));

    UPDATE pla_customer,pla_import
    SET cst_powertype = imp_arg1
    WHERE imp_class='org.powertac.common.CustomerInfo' 
      AND imp_method='withPowerType'
      AND cst_instance = imp_instance_id
      AND cst_competition = cmpid;
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'withPowerType');
    END IF;

    CALL do_log(concat_ws(',','Storing customer multi contracting',cmpid));
    
    UPDATE pla_customer,pla_import
    SET cst_multi_contracting = IF(imp_arg1='true', TRUE, FALSE)
    WHERE imp_class='org.powertac.common.CustomerInfo' 
      AND imp_method='withMultiContracting'
      AND cst_instance = imp_instance_id
      AND cst_competition = cmpid;
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'withMultiContracting');
    END IF;

    CALL do_log(concat_ws(',','Storing customer negotiation capability',cmpid));
    
    UPDATE pla_customer,pla_import
    SET cst_multi_contracting = IF(imp_arg1='true', TRUE, FALSE)
    WHERE imp_class='org.powertac.common.CustomerInfo' 
      AND imp_method='withCanNegotiate'
      AND cst_instance = imp_instance_id
      AND cst_competition = cmpid;
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'withCanNegotiate');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_market_positions
-- -----------------------------------------------------


DROP procedure IF EXISTS `import_market_positions`;

DELIMITER $$

CREATE PROCEDURE `import_market_positions` (cmpid INT, mark_processed BOOLEAN, format VARCHAR(16))
BEGIN

    # Market positions in Power TAC relate to a broker. Since gencos and buyers
    # are also brokers in Power TAC parlance, we need to connect the market positions
    # with one out of three separate tables.
    CALL do_log(concat_ws(',','Storing market positions for competition',cmpid));

	IF format = "2012" THEN
		INSERT IGNORE INTO pla_market_position
			(mpo_instance,mpo_competition,mpo_broker,mpo_genco,mpo_buyer,mpo_timeslot,mpo_initial)
		SELECT imp_instance_id, cmpid, bkr_id, gnc_id, buy_id, tsl_id, imp_mpo_initial
		FROM
			(SELECT 
				imp_instance_id,           
				convert_number(imp_arg3) imp_mpo_initial,
				convert_key(imp_arg1) imp_entity_instance,
				convert_key(imp_arg2) imp_tsl_instance
			 FROM pla_import
			 WHERE imp_class='org.powertac.common.MarketPosition' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON imp_tsl_instance = tslmap_instance
		LEFT JOIN pla_broker
			ON bkr_instance=imp_entity_instance AND bkr_competition=cmpid
		LEFT JOIN pla_genco
			ON gnc_instance=imp_entity_instance AND gnc_competition=cmpid
		LEFT JOIN pla_buyer
			ON buy_instance=imp_entity_instance AND buy_competition=cmpid
		LEFT JOIN pla_timeslot
			ON tsl_serial=tslmap_serial AND tsl_competition=cmpid;

	ELSE
		INSERT IGNORE INTO pla_market_position
			(mpo_instance,mpo_competition,mpo_broker,mpo_genco,mpo_buyer,mpo_timeslot,mpo_initial)
		SELECT imp_instance_id, cmpid, bkr_id, gnc_id, buy_id, tsl_id, imp_mpo_initial
		FROM
			(SELECT 
				imp_instance_id,           
				convert_number(imp_arg3) imp_mpo_initial,
				convert_key(imp_arg1) imp_entity_instance,
				convert_key(imp_arg2) imp_tsl_serial
			 FROM pla_import
			 WHERE imp_class='org.powertac.common.MarketPosition' AND imp_method='new') imp
		LEFT JOIN pla_broker
			ON bkr_instance=imp_entity_instance AND bkr_competition=cmpid
		LEFT JOIN pla_genco
			ON gnc_instance=imp_entity_instance AND gnc_competition=cmpid
		LEFT JOIN pla_buyer
			ON buy_instance=imp_entity_instance AND buy_competition=cmpid
		LEFT JOIN pla_timeslot
			ON tsl_serial=imp_tsl_serial AND tsl_competition=cmpid;
	END IF;
  
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.MarketPosition', 'new');
    END IF;


    CALL do_log(concat_ws(',','Storing market position changes for competition',cmpid));

    INSERT IGNORE INTO pla_market_position_update
        (mpu_market_position,mpu_mwh,mpu_timeslot)
    SELECT mpo_id, convert_number(imp_arg1) imp_mwh, tsl_id
        FROM pla_import
    LEFT JOIN pla_timeslot
        ON (tsl_serial=imp_ts AND tsl_competition=cmpid)
    LEFT JOIN pla_market_position
        ON mpo_instance=imp_instance_id AND mpo_competition=cmpid
    WHERE imp_class='org.powertac.common.MarketPosition' AND imp_method='updateBalance';

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.MarketPosition', 'updateBalance');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function convert_key
-- -----------------------------------------------------


DROP function IF EXISTS `convert_key`;

DELIMITER $$

CREATE FUNCTION `convert_key`(instring VARCHAR(256)) 
RETURNS INT
BEGIN

  DECLARE pos INTEGER;
  DECLARE s VARCHAR(256);
  SET instring = replace(instring, '-', '');

  IF(instring = 'null') THEN
    RETURN NULL;
  ELSEIF(instring = 'NaN') THEN
    RETURN NULL;
  ELSEIF(instring = 'Infinity') THEN
    RETURN NULL;
  ELSE
    RETURN CAST(TRIM(instring) AS UNSIGNED INTEGER);
  END IF;
END$$

DELIMITER ;


-- -----------------------------------------------------
-- function test_import
-- -----------------------------------------------------


DROP function IF EXISTS `test_import`;

DELIMITER $$



CREATE FUNCTION `test_import` (cmpid INT)
RETURNS VARCHAR(256)
BEGIN

    DECLARE cnt,cntimp INTEGER;
    DECLARE minval,maxval,minvalimp,maxvalimp DECIMAL(15,5);

    DECLARE dres,dres2,dresimp,dres2imp DECIMAL(15,5);

    -- -----------------------------------------------------------------------------
    --  Test competition and its properties
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*) 
    INTO cnt
    FROM pla_competition 
    WHERE cmp_id = cmpid;
        
    IF cnt != 1 THEN
        RETURN concat_ws(',','Failure in test','pla_competition');
    END IF;
      
    SELECT COUNT(*) 
    INTO cnt
    FROM pla_competition_properties
    WHERE cpr_id = cmpid;

    IF cnt = 0 THEN
        RETURN concat_ws(',','Failure in test','pla_competition_properties');
    END IF;

    -- -----------------------------------------------------------------------------
    --  Test competition and its properties
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*), MIN(tsl_serial), MAX(tsl_serial) 
    INTO cnt, minval, maxval
    FROM pla_timeslot 
    WHERE tsl_competition = cmpid;

    SELECT COUNT(*), MIN(convert_key(imp_arg1)), MAX(convert_key(imp_arg1))
    INTO cntimp, minvalimp, maxvalimp
    FROM pla_import
    WHERE imp_class='org.powertac.common.Timeslot' AND imp_method='new';

    IF cnt != cntimp OR minval != minvalimp OR maxval != maxvalimp THEN
        RETURN concat_ws(',','Failure in test','pla_timeslot');
    END IF;

    -- -----------------------------------------------------------------------------
    --  Test balancing transactions
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*),SUM(btr_kwh), SUM(btr_charge) 
    INTO cnt,dres,dres2
    FROM pla_balancing_transaction 
    WHERE btr_competition=cmpid;

    SELECT COUNT(*),SUM(imp_arg3),SUM(imp_arg4) 
    INTO cntimp,dresimp,dres2imp
    FROM pla_import
    WHERE imp_class='org.powertac.common.BalancingTransaction' AND imp_method='new';

    IF cnt != cntimp OR aeq(dres,dresimp) OR aeq(dres2,dres2imp) THEN
        RETURN concat_ws(',','Failure in test','pla_balancing_transaction');
    END IF;

    -- -----------------------------------------------------------------------------
    --  Test market transactions
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*),SUM(mtr_mwh), SUM(mtr_price) 
    INTO cnt,dres,dres2
    FROM pla_market_transaction 
    WHERE mtr_competition=cmpid;

    SELECT COUNT(*),SUM(imp_arg4),SUM(imp_arg5) 
    INTO cntimp,dresimp,dres2imp
    FROM pla_import
    WHERE imp_class='org.powertac.common.MarketTransaction' AND imp_method='new';

    IF cnt != cntimp OR aeq(dres,dresimp) OR aeq(dres2,dres2imp) THEN
        RETURN concat_ws(',','Failure in test','pla_market_transaction');
    END IF;

    RETURN "OK";

-- SELECT * FROM pla_customer;
-- SELECT * FROM pla_broker;
-- SELECT * FROM pla_genco;
-- SELECT * FROM pla_genco_state;
-- SELECT * FROM pla_tariff;
-- SELECT * FROM pla_rate;
-- SELECT * FROM pla_tariff_transaction;
-- SELECT * FROM pla_weather_forecast;
-- SELECT * FROM pla_weather_forecast_data;
-- SELECT * FROM pla_weather_report;
-- SELECT * FROM pla_order;
-- SELECT * FROM pla_orderbook;
-- SELECT * FROM pla_orderbook_order;

-- SELECT * FROM pla_cash_position;
-- SELECT * FROM pla_cash_position_deposit;
-- SELECT * FROM pla_bank_transaction;
-- SELECT * FROM pla_buyer;
-- SELECT * FROM pla_buyer_state;
-- SELECT * FROM pla_distribution_transaction;

-- SELECT * FROM pla_market_position;
-- SELECT * FROM pla_market_position_update;
-- SELECT * FROM pla_order;
-- SELECT * FROM pla_cleared_trade;


END$$

DELIMITER ;

-- -----------------------------------------------------
-- function bootstrap_length
-- -----------------------------------------------------


DROP function IF EXISTS `bootstrap_length`;

DELIMITER $$



CREATE FUNCTION `bootstrap_length` (cmpid INT)
RETURNS INT
BEGIN
    DECLARE start INT;

    SELECT SUM(timeslots) offset 
    INTO start
    FROM
        (SELECT
            CAST(AVG(convert_number(cpr_value)) AS UNSIGNED INTEGER) timeslots
        FROM pla_competition_properties
        WHERE
            cpr_competition = cmpid AND
            cpr_name IN ('BootstrapTimeslotCount', 'BootstrapDiscardedTimeslots')
        GROUP BY cpr_name) a;
    
    RETURN start;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure delete_competition
-- -----------------------------------------------------


DROP procedure IF EXISTS `delete_competition`;

DELIMITER $$



CREATE PROCEDURE `delete_competition` (cmpid INT)
BEGIN

    DELETE FROM pla_balancing_transaction WHERE btr_competition = cmpid;
    DELETE FROM pla_bank_transaction WHERE bnt_competition = cmpid;
    DELETE FROM pla_cash_position WHERE cpo_competition = cmpid;
    DELETE FROM pla_cleared_trade WHERE clt_competition = cmpid;
    DELETE FROM pla_distribution_transaction WHERE dtr_competition = cmpid;
    DELETE FROM pla_market_position WHERE mpo_competition = cmpid;
    DELETE FROM pla_market_transaction WHERE mtr_competition = cmpid;
    DELETE FROM pla_order WHERE ord_competition = cmpid;
    DELETE FROM pla_orderbook WHERE obk_competition = cmpid;
    DELETE FROM pla_tariff WHERE trf_competition = cmpid;
    DELETE FROM pla_weather_forecast WHERE wfc_competition = cmpid;
    DELETE FROM pla_weather_report WHERE wre_competition = cmpid;
    DELETE FROM pla_customer WHERE cst_competition = cmpid;
    DELETE FROM pla_buyer WHERE buy_competition = cmpid;
    DELETE FROM pla_genco WHERE gnc_competition = cmpid;
    DELETE FROM pla_broker WHERE bkr_competition = cmpid;
    DELETE FROM pla_timeslot WHERE tsl_competition = cmpid;
    DELETE FROM pla_competition_properties WHERE cpr_competition = cmpid;
    DELETE FROM pla_competition WHERE cmp_id = cmpid;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function aeq
-- -----------------------------------------------------


DROP function IF EXISTS `aeq`;

DELIMITER $$



CREATE FUNCTION `aeq` (v1 DECIMAL, v2 DECIMAL)
RETURNS BOOLEAN
BEGIN

    DECLARE eps DECIMAL(5,5) DEFAULT 0.1;

    IF v1 > v2 + eps OR v1 < v2 - eps THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;

END$$

DELIMITER ;




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
